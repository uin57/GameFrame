/**
 * 
 */
package com.app.testmodel;

import com.app.annotation.Table;
import com.app.service.item.UserItem;

/**
 * @author lisong
 * @version 2013-4-25 上午10:27:36
 */
@Table
public class PlayerItem extends UserItem {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2870488051000386139L;
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
