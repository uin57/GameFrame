/**
 * 
 */
package com.app.testmodel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.app.annotation.Table;
import com.app.framework.exception.ORMException;
import com.app.framework.orm.Collection;
import com.app.framework.util.IdxUtil;
import com.app.framework.util.StringUtils;


/**
 * @author lisong
 * @version 2013-5-6 下午1:56:44
 */
@Table
public class PlayerItemCollection extends Collection<PlayerItem> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2064323449784767634L;

	/**
	 * key 设定id value 实例化的uid
	 */
	private Map<String,List<Long>> designId2uid = new HashMap<String, List<Long>>();
	
	@Override
	public PlayerItem putItem(PlayerItem item) throws Exception {
		String itemId = item.getItemId();
		if(StringUtils.isNullOrEmpty(itemId)) {
			throw new ORMException("设定id不能为空");
		}
		if(!designId2uid.containsKey(itemId)) {
			designId2uid.put(itemId, new ArrayList<Long>());
		}
		if(item.getId() == 0) {
			item.setId(IdxUtil.nextId(item.getClass()));
		}
		designId2uid.get(itemId).add(item.getId());
		return super.putItem(item);
	}
	
	@Override
	public void remove(PlayerItem item) throws Exception {
		designId2uid.get(item.getItemId()).remove(item.getId());
		super.remove(item);
	}
	@Override
	public void clear() throws ORMException {
		designId2uid.clear();
		super.clear();
	}
}
