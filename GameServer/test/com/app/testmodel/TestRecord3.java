/**
 * 
 */
package com.app.testmodel;

import com.app.annotation.Table;
import com.app.framework.annotations.Index;
import com.app.framework.orm.ActiveRecord;

/**
 * @author lisong
 * @version 2013-3-27 上午10:51:12
 */
@Table
public class TestRecord3 extends ActiveRecord {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TestRecord3() {
	}
	
	public TestRecord3(long id,String name,int age) {
		this.id = id;
		this.name = name;
		this.age =age;
	}
	
	private String name;
	@Index
	private int age;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
}
