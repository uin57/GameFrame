/**
 * 
 */
package com.app.testmodel;

import com.app.annotation.Table;
import com.app.framework.annotations.Index;
import com.app.framework.annotations.Save;
import com.app.framework.orm.ActiveRecord;

/**
 * @author lisong
 * @version 2013-3-27 上午10:44:50
 */
@Table
public class TestRecord2 extends ActiveRecord {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public TestRecord2() {
	}
	public TestRecord2(long id,String name,TestRecord3 rd3) {
		this.id = id;
		this.name = name;
		this.ref = rd3;
		this.val =rd3;
		this.none = rd3;
	}
	@Index
	private String name;
	
	private TestRecord3 none;
	@Save(type="reference")
	private TestRecord3 ref;
	@Save(type="value")
	private TestRecord3 val;
	
	public TestRecord3 getNone() {
		return none;
	}
	public void setNone(TestRecord3 none) {
		this.none = none;
	}
	public TestRecord3 getRef() {
		return ref;
	}
	public void setRef(TestRecord3 ref) {
		this.ref = ref;
	}
	public TestRecord3 getVal() {
		return val;
	}
	public void setVal(TestRecord3 val) {
		this.val = val;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
