/**
 * 
 */
package com.app.testmodel;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;

import com.app.annotation.Table;
import com.app.framework.annotations.Save;
import com.app.framework.orm.ActiveRecord;
import com.app.framework.util.IdxUtil;

/**
 * @author lisong
 * @version 2013-3-26 下午1:06:21
 */
@Table
public class TestRecord extends ActiveRecord {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String name;
	
	private TestRecord2 none;
	@Save(type="reference")
	private TestRecord2 ref;
	@Save(type="value")
	private TestRecord2 val;
	@Save(type="reference")
	private List<TestRecord2> lstref;
	@Save(type="value")
	private List<TestRecord2> lstval;
	@Save(type="reference")
	private Map<Integer,TestRecord2> mapref;
	@Save(type="value")
	private Map<Integer,TestRecord2> mapval;
	private String[] tmp;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public TestRecord2 getNone() {
		return none;
	}
	public void setNone(TestRecord2 none) {
		this.none = none;
	}
	public TestRecord2 getRef() {
		return ref;
	}
	public void setRef(TestRecord2 ref) {
		this.ref = ref;
	}
	public TestRecord2 getVal() {
		return val;
	}
	public void setVal(TestRecord2 val) {
		this.val = val;
	}
	public List<TestRecord2> getLstref() {
		return lstref;
	}
	public void setLstref(List<TestRecord2> lstref) {
		this.lstref = lstref;
	}
	public List<TestRecord2> getLstval() {
		return lstval;
	}
	public void setLstval(List<TestRecord2> lstval) {
		this.lstval = lstval;
	}
	public Map<Integer, TestRecord2> getMapref() {
		return mapref;
	}
	public void setMapref(Map<Integer, TestRecord2> mapref) {
		this.mapref = mapref;
	}
	public Map<Integer, TestRecord2> getMapval() {
		return mapval;
	}
	public void setMapval(Map<Integer, TestRecord2> mapval) {
		this.mapval = mapval;
	}
	
	public String[] getTmp() {
		return tmp;
	}
	public void setTmp(String[] tmp) {
		this.tmp = tmp;
	}
	public static void main(String[] args) throws Exception {
		Semaphore s = new Semaphore(1);
		for(int i=0;i<3;i++) {
			s.release();
		}
		System.out.println(s.availablePermits());
//		for(int i=0;i<500;i++) {
//			Thread t = new Thread(new Runnable() {
//				
//				@Override
//				public void run() {
//					for(int j=0;j<1000;j++){
//						IdxUtil.nextId(TestRecord.class);
//					}
//				}
//			});
//			t.start();
//			t.join();
//		}
	}
}
