package com.app;

import java.util.Map;

import com.app.annotation.Table;
import com.app.framework.annotations.Index;
import com.app.framework.annotations.Save;
import com.app.framework.orm.ActiveRecord;

@Table
public class Test2 extends ActiveRecord{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1466538903732483200L;

	@Index
	private String b1;
	
	private boolean b2;
	
	@Save(type="value")
	private Test1 t1;
	
//	@Save(type ="reference")
//	private List<Test1> t1List;

	@Save(type ="reference")
	private Map<String,Object> t1List;
	

	public String getB1() {
		return b1;
	}

	public void setB1(String b1) {
		this.b1 = b1;
	}


	public boolean isB2() {
		return b2;
	}

	public void setB2(boolean b2) {
		this.b2 = b2;
	}

	public Test1 getT1() {
		return t1;
	}

	public void setT1(Test1 t1) {
		this.t1 = t1;
	}

	public Map<String, Object> getT1List() {
		return t1List;
	}

	public void setT1List(Map<String, Object> t1List) {
		this.t1List = t1List;
	}

//	public List<Test1> getT1List() {
//		return t1List;
//	}
//
//	public void setT1List(List<Test1> t1List) {
//		this.t1List = t1List;
//	}

	
	
	
	
}
