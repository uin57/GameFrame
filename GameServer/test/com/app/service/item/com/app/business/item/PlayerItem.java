package com.app.service.item.com.app.business.item;

import com.app.service.item.UserItem;
import com.app.framework.annotations.TransferInvisible;
import com.app.framework.annotations.Index;
import com.app.framework.annotations.Transient;

public class PlayerItem extends UserItem {

	/**
	  *计算得来的攻击
	  */	
  	@TransferInvisible	
	private int runtimeAtk;
				
	/**
	  *计算得来的防御
	  */	
  	@TransferInvisible	
	private int runtimeDef;
				
	/**
	  *孔
	  */	
	private int[] hole;
				
	/**
	  *加强值
	  */	
  	@Index	
	private int strengthen;
				
	/**
	  *临时值 不存数据库
	  */	
  	@Transient	
	private String temp;
				
	/**
	  *用于服务器端计算 不下发客户端
	  */	
  	@TransferInvisible	
	private int calcValue;
				

	public int getRuntimeAtk() {
		return this.runtimeAtk;
	}	
	
	public void setRuntimeAtk(int runtimeAtk ) {
		this.runtimeAtk = runtimeAtk;
	}
	
	public int getRuntimeDef() {
		return this.runtimeDef;
	}	
	
	public void setRuntimeDef(int runtimeDef ) {
		this.runtimeDef = runtimeDef;
	}
	
	public int[] getHole() {
		return this.hole;
	}	
	
	public void setHole(int[] hole ) {
		this.hole = hole;
	}
	
	public int getStrengthen() {
		return this.strengthen;
	}	
	
	public void setStrengthen(int strengthen ) {
		this.strengthen = strengthen;
	}
	
	public String getTemp() {
		return this.temp;
	}	
	
	public void setTemp(String temp ) {
		this.temp = temp;
	}
	
	public int getCalcValue() {
		return this.calcValue;
	}	
	
	public void setCalcValue(int calcValue ) {
		this.calcValue = calcValue;
	}
	
}

