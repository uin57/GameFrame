package com.app.service.item.com.app.business.skill;

import com.app.service.item.UserItem;

public class PlayerSkill extends UserItem {

	private int level;
				

	public int getLevel() {
		return this.level;
	}	
	
	public void setLevel(int level ) {
		this.level = level;
	}
	
}

