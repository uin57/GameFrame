package com.app.service.item.com.app.business.task;

import com.app.service.item.UserItem;

public class PlayerTask extends UserItem {

	/**
	  *任务状态
	  */	
	private int status;
				
	/**
	  *完成条件
	  */	
	private String[][] comp;
				
	/**
	  *接受条件
	  */	
	private String[] accept;
				

	public int getStatus() {
		return this.status;
	}	
	
	public void setStatus(int status ) {
		this.status = status;
	}
	
	public String[][] getComp() {
		return this.comp;
	}	
	
	public void setComp(String[][] comp ) {
		this.comp = comp;
	}
	
	public String[] getAccept() {
		return this.accept;
	}	
	
	public void setAccept(String[] accept ) {
		this.accept = accept;
	}
	
}

