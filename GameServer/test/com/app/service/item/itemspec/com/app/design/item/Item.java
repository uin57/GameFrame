package com.app.service.item.itemspec.com.app.design.item;

import com.app.service.item.itemspec.ItemSpec;

public class Item extends ItemSpec {

	/**
	  *等级
	  */	
	private int level;
				
	/**
	  *类型
	  */	
	private int type;
				

	public int getLevel() {
		return this.level;
	}	
	
	public void setLevel(int level ) {
		this.level = level;
	}
	
	public int getType() {
		return this.type;
	}	
	
	public void setType(int type ) {
		this.type = type;
	}
	
}

