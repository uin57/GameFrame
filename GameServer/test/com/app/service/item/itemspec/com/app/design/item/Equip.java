package com.app.service.item.itemspec.com.app.design.item;

import com.app.service.item.itemspec.com.app.design.item.Item;

public class Equip extends Item {

	/**
	  *攻击
	  */	
	private int atk;
				
	/**
	  *防御
	  */	
	private int def;
				

	public int getAtk() {
		return this.atk;
	}	
	
	public void setAtk(int atk ) {
		this.atk = atk;
	}
	
	public int getDef() {
		return this.def;
	}	
	
	public void setDef(int def ) {
		this.def = def;
	}
	
}

