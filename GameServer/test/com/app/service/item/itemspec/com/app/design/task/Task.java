package com.app.service.item.itemspec.com.app.design.task;

import com.app.service.item.itemspec.ItemSpec;

public class Task extends ItemSpec {

	/**
	  *需要等级
	  */	
	private int level;
				
	/**
	  *任务类型
	  */	
	private int type;
				

	public int getLevel() {
		return this.level;
	}	
	
	public void setLevel(int level ) {
		this.level = level;
	}
	
	public int getType() {
		return this.type;
	}	
	
	public void setType(int type ) {
		this.type = type;
	}
	
}

