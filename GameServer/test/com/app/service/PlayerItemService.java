/**
 * 
 */
package com.app.service;

import java.util.Map;

import com.app.framework.annotations.ServiceClientVisible;
import com.app.framework.http.HttpContext;
import com.app.framework.result.RequestResult;
import com.app.framework.service.GameService;
import com.app.framework.util.JSONUtil;
import com.app.testmodel.PlayerItem;
import com.app.testmodel.PlayerItemCollection;

/**
 * @author lisong
 * @version 2013-3-5 上午11:48:54
 */
public class PlayerItemService implements GameService {


	@SuppressWarnings("unchecked")
	@ServiceClientVisible
	public RequestResult<String> add(HttpContext context) throws Exception {
		Map<String,String> param = JSONUtil.toMap(context.getContent());
		long userId = Long.parseLong(param.get("userId"));
		PlayerItemCollection uc = context.getPerssistenceSession().get(userId, PlayerItemCollection.class);
		if(uc == null) {
			uc = new PlayerItemCollection();
			uc.setId(userId);
		}
		PlayerItem pi = new PlayerItem();
		pi.setItemId("test1");
		uc.putItem(pi);
		context.getPerssistenceSession().put(uc);
		
		return RequestResult.success("success"+param.get("test2"));
	}
	
	@SuppressWarnings("unchecked")
	@ServiceClientVisible
	public RequestResult<String> remove(HttpContext context) throws Exception {
		Map<String,String> param = JSONUtil.toMap(context.getContent());
		long userId = Long.parseLong(param.get("userId"));
		long itemId = Long.parseLong(param.get("itemId"));
		PlayerItemCollection uc = context.getPerssistenceSession().get(userId, PlayerItemCollection.class);
		uc.remove(itemId, PlayerItem.class);
		return RequestResult.success("success");
	}
	
	@SuppressWarnings("unchecked")
	@ServiceClientVisible
	public RequestResult<String> removeCollection(HttpContext context) throws Exception {
		Map<String,String> param = JSONUtil.toMap(context.getContent());
		String userId = param.get("userId");
		PlayerItemCollection uc = context.getPerssistenceSession().get(Long.parseLong(userId), PlayerItemCollection.class);
		context.getPerssistenceSession().remove(uc);
		return RequestResult.success("success");
	}

	
}
