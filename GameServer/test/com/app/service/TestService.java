/**
 * 
 */
package com.app.service;

import com.app.framework.annotations.ServiceClientVisible;
import com.app.framework.context.Request;
import com.app.framework.http.HttpContext;
import com.app.framework.result.RequestResult;
import com.app.framework.service.GameService;
import com.app.framework.singleton.ServiceSingletonManager;
import com.app.gameserver.service.http.SessionService;

/**
 * @author lisong
 * @version 2013-3-5 上午11:48:54
 */
public class TestService implements GameService {


	@ServiceClientVisible
	public RequestResult<String> test(HttpContext context) throws Exception {
		
		System.out.println("------------执行");
		SessionService service = (SessionService) ServiceSingletonManager.getInstance().getSingleton(Request.SESSION_SERVICE_ID);
		service.createSession(123, context);
		
		return RequestResult.success("success");
	}
	
}
