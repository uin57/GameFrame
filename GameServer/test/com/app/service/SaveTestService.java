package com.app.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.cglib.proxy.Enhancer;
import com.app.Test3;
import com.app.framework.annotations.ServiceClientVisible;
import com.app.framework.context.AbstractContext;
import com.app.framework.exception.JSONException;
import com.app.framework.exception.ORMException;
import com.app.framework.orm.session.PersistenceSession;
import com.app.framework.orm.session.Query;
import com.app.framework.result.RequestResult;
import com.app.framework.service.GameService;
import com.app.framework.util.IdxUtil;
import com.app.testmodel.TestRecord;
import com.app.testmodel.TestRecord2;
import com.app.testmodel.TestRecord3;

public class SaveTestService implements GameService{

	@ServiceClientVisible
	public RequestResult save(AbstractContext context) {
//		Map<String,String> json = context.getParameters();
//		String src = json.get("test1");
//		try {
//			Map<String,String> jsonObject = JSONUtil.toMap(src);
//			StreamHelper stream = new StreamHelper(context.getPerssistenceSession());
//			Test1 tt= (Test1) stream.josn2Object(jsonObject, Test1.class);
//			System.out.println(tt.getA1());
//			System.out.println(tt.getA2());
//			System.out.println(tt.getA3List());
//		} catch (JSONException e) {
//			e.printStackTrace();
//		} catch (ORMException e) {
//			e.printStackTrace();
//		}
		
		
//		Test1 t1a = new Test1();
//		t1a.setUid("101");
//		t1a.setA1("t1a");
//		t1a.setA2(20);
//		List<String> ta1List = new ArrayList<String>();
//		ta1List.add("t1a1");
//		ta1List.add("t1a2");
//		t1a.setA3List(ta1List);
//		
//		Test1 t1b = new Test1();
//		t1b.setUid("102");
//		t1b.setA1("t1b");
//		t1b.setA2(20);
//		List<String> t1bList = new ArrayList<String>();
//		t1bList.add("t1b1");
//		t1bList.add("t1b2");
//		t1a.setA3List(t1bList);
//		
//		
//		Test2 t2 = new Test2();
//		t2.setUid("002");
//		t2.setB1("b1");
//		t2.setT1(t1a);
////		List<Test1> t1List = new ArrayList<Test1>();
////		t1List.add(t1a);
////		t1List.add(t1b);
//		Map<String, Object> t1List = new HashMap<String, Object>();
//		t1List.put(t1a.getUid(), t1a);
//		t1List.put(t1b.getUid(), t1b);
//		
//		
//		t2.setT1List(t1List);
		
		
		
		
//		StreamHelper stream = new StreamHelper(context.getPerssistenceSession());
//		try {
//			Map<String, Object> jsonMap = stream.object2josn(Test2.class, t2, new ArrayList<String>(), new ArrayList<String>(), new ArrayList<String>());
//			for(String key :jsonMap.keySet()) {
//				System.out.println(jsonMap.get(key));
//			}
//		} catch (ORMException e) {
//			e.printStackTrace();
//		}
		
		PersistenceSession session = context.getPerssistenceSession();
		try {
//			session.put(t2);
			Test3 tt = new Test3();
			tt.setId(IdxUtil.nextId(Test3.class));
			session.put(tt);
			session.flush();
			Test3 t3 = session.get(3, Test3.class);
			System.out.println(t3.getClass().getName());
			System.out.println("iscglib:"+Enhancer.class.isAssignableFrom(t3.getClass()));
			t3.setId(3);
			session.put(t3);
			session.flush();
			Query q = new Query(TestRecord.class);
			List<TestRecord> lst = (List<TestRecord>) session.query(q);
			lst.get(0);
//			Test2 tt = session.get("002", Test2.class);
//			session.put(tt);
//			Test1 tt1 = tt.getT1();
//			System.out.println(tt1);
//			System.out.println(tt1.getUid());
//			System.out.println(tt1.getA1());
//			System.out.println(tt1.getA3List());
			
//			List<Test1> tt2=tt.getT1List();
//			for(Test1 t : tt2) {
//				System.out.println(t);
//				System.out.println(t.getUid());
//				System.out.println(t.getA1());
//				System.out.println(t.getA3List());
//			}
			
//			session.remove("002", Test2.class);
			
		}  catch (Exception e) {
			e.printStackTrace();
		} 
//		System.out.println(context.getParameters());
//		System.out.println("------------执行");
		return RequestResult.success("save do");
	}
	
	@ServiceClientVisible
	public RequestResult saveCascade(AbstractContext context) throws ORMException {

		TestRecord rd = new TestRecord();
		rd.setName("正常情况下");
		rd.setNone(new TestRecord2(IdxUtil.nextId(TestRecord2.class),"rd1-none-rd2",new TestRecord3(IdxUtil.nextId(TestRecord3.class), "rd2-rd3", 1)));
		rd.setRef(new TestRecord2(IdxUtil.nextId(TestRecord2.class),"rd1-ref-rd2",new TestRecord3(IdxUtil.nextId(TestRecord3.class), "rd2-rd3", 2)));
		rd.setVal(new TestRecord2(IdxUtil.nextId(TestRecord2.class),"rd1-val-rd2",new TestRecord3(IdxUtil.nextId(TestRecord3.class), "rd2-rd3", 3)));
		
		List<TestRecord2> lstref = new ArrayList<TestRecord2>();
		
		lstref.add(new TestRecord2(IdxUtil.nextId(TestRecord2.class),"lstref1-rd2",new TestRecord3(IdxUtil.nextId(TestRecord3.class), "rd2-rd3", 4)));
		lstref.add(new TestRecord2(IdxUtil.nextId(TestRecord2.class),"lstref2-rd2",new TestRecord3(IdxUtil.nextId(TestRecord3.class), "rd2-rd3", 5)));
		rd.setLstref(lstref);
		
		List<TestRecord2> lstval = new ArrayList<TestRecord2>();
		lstval.add(new TestRecord2(IdxUtil.nextId(TestRecord2.class),"listvaluerd2-rd3",new TestRecord3(IdxUtil.nextId(TestRecord3.class),"",6)));
		lstval.add(new TestRecord2(IdxUtil.nextId(TestRecord2.class),"listvaluerd2-rd3",new TestRecord3(IdxUtil.nextId(TestRecord3.class),"",7)));
		rd.setLstval(lstval);
		
		Map<Integer,TestRecord2> mapref = new HashMap<Integer, TestRecord2>();
		mapref.put(1,new TestRecord2(IdxUtil.nextId(TestRecord2.class),"mapref1-rd2",new TestRecord3(IdxUtil.nextId(TestRecord3.class), "rd2-rd3", 8)));
		mapref.put(2,new TestRecord2(IdxUtil.nextId(TestRecord2.class),"mapref2-rd2",new TestRecord3(IdxUtil.nextId(TestRecord3.class), "rd2-rd3", 9)));
		rd.setMapref(mapref);
		
		Map<Integer,TestRecord2> mapval = new HashMap<Integer, TestRecord2>();
		mapval.put(3,new TestRecord2(IdxUtil.nextId(TestRecord2.class),"mapref1-rd2",new TestRecord3(IdxUtil.nextId(TestRecord3.class), "rd2-rd3", 10)));
		mapval.put(4,new TestRecord2(IdxUtil.nextId(TestRecord2.class),"mapref2-rd2",new TestRecord3(IdxUtil.nextId(TestRecord3.class), "rd2-rd3", 11)));
		rd.setMapval(mapval);

		String[] tmp = new String[2];
		tmp[0] = "0";
		tmp[1] = "1";
		rd.setTmp(tmp);
		rd.setId(1);
		context.getPerssistenceSession().put(rd);
		return RequestResult.success(rd);
	}
	
	@ServiceClientVisible
	public RequestResult getCascade(AbstractContext context) throws ORMException, JSONException {

		TestRecord rd = context.getPerssistenceSession().get(1, TestRecord.class);
		rd.setName("aaa");
		return RequestResult.success(rd);
	}
}
