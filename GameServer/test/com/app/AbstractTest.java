/**
 * 
 */
package com.app;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import com.app.framework.context.ActionRequest;
import com.app.framework.context.Request;
import com.app.framework.http.HttpServiceRequest;
import com.app.framework.util.Base64Util;
import com.app.framework.util.JSONUtil;

/**
 * @author lisong
 * @version 2013-2-27 下午3:14:12
 */
public abstract class AbstractTest {

	String uri = "http://127.0.0.1:9090/platform?tt=t";
	/**
	 * header 参数
	 * 
	 * header 参数 请求字符串的名称  cmd
	 * header 参数 参数json串长度的名称 params-length
	 * content中请求的参数json串 params
	 * 
	 * 返回:
	 * 请求字符串的名称  cmd
	 * 参数json串长度的名称 result-length
	 * 返回的数据 result
	 * 请求序列号 reqId
	 * @param request
	 * @throws Exception
	 */
	public void test(Request request) throws Exception {
		DefaultHttpClient httpClient=new DefaultHttpClient();
		try {
			httpClient.getParams().setParameter(ClientPNames.COOKIE_POLICY,  CookiePolicy.BROWSER_COMPATIBILITY); //s设置cookie的兼容性
			HttpPost httpPost=new HttpPost(uri);
			if(request.getType() == 2) {
				httpPost.addHeader("action", ""+((ActionRequest) request).getActionId());
				httpPost.addHeader("user", ((ActionRequest) request).getUserId()+"");
				httpPost.addHeader("reqid", ((ActionRequest) request).getRequestId()+"");
			}
			else {
				httpPost.addHeader("service",""+((HttpServiceRequest)request).getServiceId());
				httpPost.addHeader("method", ((HttpServiceRequest)request).getMethodName());
			}
			
			httpPost.addHeader("cmd", request.getType()+"");
			byte[] bt = request.getContent();
			if(bt == null) {
				bt = new byte[0];
			}
			HttpEntity entity = new ByteArrayEntity(bt);
			httpPost.setEntity(entity);
			long b = System.currentTimeMillis();
			HttpResponse response = httpClient.execute(httpPost);
			
			StatusLine sl = response.getStatusLine(); 
			if(sl.getStatusCode() != 200) {
				System.out.println("http code:"+sl.getStatusCode() +" msg:"+sl.getReasonPhrase());
				return;
			}
			System.out.println("httpclient exe:"+(System.currentTimeMillis() - b));
			
			HttpEntity rsp = response.getEntity();
			
			InputStream in = null;

			in = new GZIPInputStream(rsp.getContent());
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(in,"UTF-8"));
			
			String result = reader.readLine();
			
			System.out.println("result:"+result);
			
			System.out.println("result.length:"+result.length());
			Header h4 = response.getFirstHeader("reqid");
			if(h4 != null) {
				System.out.println("last-reqId:"+h4.getValue());
			}
			httpPost.releaseConnection();
		}catch(Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			httpClient.getConnectionManager().shutdown();
		}
	}
	
	public static void main(String[] args) throws Exception {
		Map<String,String> map = new HashMap<String, String>();
		map.put("test1", "我是12中文1");
		map.put("test2", "我是中文2");
		String json = JSONUtil.toJSON(map);
		System.out.println(json.length());
		System.out.println(Base64Util.encode(json).length());
	}
}
