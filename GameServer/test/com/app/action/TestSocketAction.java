/**
 * 
 */
package com.app.action;

import java.util.concurrent.atomic.AtomicLong;

import com.app.framework.socket.Packet;
import com.app.framework.socket.SocketContext;
import com.app.gameserver.SocketServer;
import com.app.service.action.SocketAction;

/**
 * @author lisong
 * @version 2013-3-8 下午3:36:11
 */
public class TestSocketAction extends SocketAction {
	
	static AtomicLong ato = new AtomicLong();
	
	public void execute(SocketContext context) throws Exception {
		String str = new String(context.getContent());
		if(ato.incrementAndGet()%10000 == 0) {
			logger.error("收到客户端消息,发送给world并且返回给客户端:"+ato.get()+" str:"+str);
		}
		if(str.startsWith("stop")) {
			logger.error("clientstop.........."+str);
		}
		Packet p = new Packet((short)2, ("长连接:"+str).getBytes("UTF-8"));
		context.sendResponse(p);
		SocketServer.getClient().sendMessage(p);
	}
}
