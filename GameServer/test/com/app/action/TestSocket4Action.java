/**
 * 
 */
package com.app.action;

import java.util.concurrent.atomic.AtomicLong;

import com.app.framework.socket.SocketContext;
import com.app.service.action.SocketAction;

/**
 * @author lisong
 * @version 2013-3-8 下午3:36:11
 */
public class TestSocket4Action extends SocketAction {
	static final AtomicLong ato = new AtomicLong();
	public void execute(SocketContext context) throws Exception {
		String str = new String(context.getContent());
		if(ato.incrementAndGet() % 10000 == 0) {
			logger.error("server 收到world消息:"+str+"----"+ato.get());
		}
	}
}
