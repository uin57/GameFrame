/**
 * 
 */
package com.app.action;

import org.jboss.netty.channel.ChannelFuture;

import com.app.framework.socket.Packet;
import com.app.framework.socket.SocketContext;
import com.app.framework.socket.SocketResponse;
import com.app.framework.socket.manager.PlayerManager;
import com.app.framework.socket.sender.Player;
import com.app.service.action.SocketAction;

/**
 * @author lisong
 * @version 2013-3-8 下午3:36:11
 */
public class TestSocket2Action extends SocketAction {

	@SuppressWarnings("unchecked")
	public void execute(SocketContext context) throws Exception {
		String str = new String(context.getContent());
		long id = Long.parseLong(str);
		Player player = ((SocketResponse<Player>)context.getResponse()).getSender();
		player.setId(id);
		Player old = PlayerManager.getInstance().addSender(player);
System.out.println("new player:"+player.getId()+" size:"+player.getManager().size());
		if(old != null) {
			Packet last = new Packet((short)2,"异地登录".getBytes());
			//异地登录
			ChannelFuture cf = old.sendMessage(last);
			if(cf != null) {
				cf.awaitUninterruptibly();
			}
			old.disconnect();
		}
	}
}
