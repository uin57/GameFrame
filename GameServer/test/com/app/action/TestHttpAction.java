/**
 * 
 */
package com.app.action;

import java.util.Map;

import com.app.framework.http.HttpContext;
import com.app.framework.result.RequestResult;
import com.app.framework.util.JSONUtil;
import com.app.service.action.HttpAction;

/**
 * @author lisong
 * @version 2013-3-8 下午3:36:11
 */
public class TestHttpAction extends HttpAction {

	@Override
	public RequestResult<String> execute(HttpContext context)
			throws Exception {
		long userid = context.getUserId();
		Map<String,String> map = JSONUtil.toMap(context.getContent());
		System.out.println(userid+" params:"+map);
		return RequestResult.success("success");
	}

	
}
