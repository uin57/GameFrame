/**
 * 
 */
package com.app.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.app.framework.socket.Packet;
import com.app.framework.socket.SocketContext;
import com.app.framework.util.IdxUtil;
import com.app.service.action.SocketAction;
import com.app.testmodel.TestRecord;
import com.app.testmodel.TestRecord2;
import com.app.testmodel.TestRecord3;

/**
 * @author lisong
 * @version 2013-3-8 下午3:36:11
 */
public class TestSocket3Action extends SocketAction {

	public void execute(SocketContext context) throws Exception {
		Random r = new Random();
		int rcid = r.nextInt((int)IdxUtil.nextId(TestRecord.class) - 1);
		TestRecord test = context.getPerssistenceSession().get(rcid, TestRecord.class);
		if(test != null) {
			test.setName("newname");
			List<TestRecord2> lst = test.getLstref();
			if(lst.size() == 0) {
				logger.error("get:"+test.getClassname()+":"+test.getId());
			}
			else {
				lst.get(0).setName("newname");
				context.getPerssistenceSession().put(test);
			}
			
		}
		
		String str = new String(context.getContent());
		Packet p = new Packet((short)1, "测试 长连接".getBytes("UTF-8"));
		context.sendResponse(p);
		TestRecord tr = new TestRecord();
		tr.setId(IdxUtil.nextId(tr.getClass()));
		List<TestRecord2> lstref = new ArrayList<TestRecord2>();
		TestRecord3 tr3 = new TestRecord3(IdxUtil.nextId(TestRecord3.class),"rd3",1);
 		lstref.add(new TestRecord2(IdxUtil.nextId(TestRecord2.class), "lstref", tr3));
		tr.setLstref(lstref);
		tr.setLstval(lstref);
		Map<Integer, TestRecord2> mapref = new HashMap<Integer, TestRecord2>();
		TestRecord2 tr2 = new TestRecord2(IdxUtil.nextId(TestRecord2.class), "mapref", new TestRecord3(IdxUtil.nextId(TestRecord3.class),"rd3",1));
		mapref.put((int)tr2.getId(), tr2);
		tr.setMapref(mapref);
		tr.setMapval(mapref);
		tr.setName("testrecord");
		tr.setTmp(new String[]{"1","2"});
		tr.setNone(tr2);
		tr.setRef(tr2);
		tr.getRef().getNone().setAge(1);
		tr.getRef().getNone().setName("traa");
		context.getPerssistenceSession().put(tr);
	}
}
