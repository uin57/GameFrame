package com.app;

import java.util.List;

import com.app.annotation.Table;
import com.app.framework.annotations.Index;
import com.app.framework.annotations.Transient;
import com.app.framework.orm.ActiveRecord;

@Table
public class Test1  extends ActiveRecord{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1200889093966847582L;
	@Index
	private String a1;
	@Transient
	private int a2;
	
	private List<String> a3List;
	
    
	private String ownerProperty = "test1s";

	public String getOwnerProperty(){
		return this.ownerProperty;
	}
	
	public void setOwnerProperty(String ownerProperty){
		this.ownerProperty = ownerProperty;
	}
	

	public String getA1() {
		return a1;
	}
	
	public void setA1(String a1) {
		this.a1 = a1;
	}
	

	public int getA2() {
		return a2;
	}
	
	public void setA2(int a2) {
		this.a2 = a2;
	}

	public List<String> getA3List() {
		return a3List;
	}

	public void setA3List(List<String> a3List) {
		this.a3List = a3List;
	}
	
	
	
}

