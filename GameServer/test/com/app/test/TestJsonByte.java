/**
 * 
 */
package com.app.test;

import org.junit.Test;

import com.app.framework.orm.StreamHelper;
import com.app.framework.util.JSONUtil;

/**
 * @author lisong
 * @version 2013-3-21 下午4:23:06
 */
public class TestJsonByte {

	@Test
	public void testTojson() throws Exception {
		//StreamHelper sh =StreamHelper.getInstance();
		String[][] arr = new String[5][];
		String[] tmp1 = new String[2];
		tmp1[0] = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
		tmp1[1] = "bbbaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
		arr[0] = tmp1;
		String[] tmp2 = new String[2];
		tmp1[0] = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
		tmp1[1] = "bbbaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
		arr[1] = tmp2;
		String[] tmp3 = new String[2];
		tmp1[0] = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
		tmp1[1] = "bbbaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
		arr[2] = tmp3;
		JSONUtil.toJSON("asd");//初始化json工具
		
		System.out.println(JSONUtil.toJSON(arr));
	//	System.out.println(sh.object2bytes(arr));
		long b = System.currentTimeMillis();
		for(int i=0;i<100000;i++) {
			//JSONUtil.toJSON(arr);
			//sh.object2bytes(arr);
		}
		
		System.out.println(System.currentTimeMillis() - b);
	}
	/**
	 * bytes2object循环十万次 花费1.7秒
	 * jsontoobject循环十万次花费0.7秒
	 * @throws Exception
	 */
	@Test
	public void testToObject() throws Exception {
		JSONUtil.toJSON("asd");//初始化json工具
	//	StreamHelper sh = StreamHelper.getInstance();
		String[][] arr = new String[5][];
		String[] tmp1 = new String[2];
		tmp1[0] = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
		tmp1[1] = "bbbaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
		arr[0] = tmp1;
		String[] tmp2 = new String[2];
		tmp1[0] = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
		tmp1[1] = "bbbaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
		arr[1] = tmp2;
		String[] tmp3 = new String[2];
		tmp1[0] = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
		tmp1[1] = "bbbaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
		arr[2] = tmp3;
		String json = JSONUtil.toJSON(arr);
		//byte[] by = sh.object2bytes(arr);
		//String[][] tmp = JSONUtil.toObject(json, (Class<String[][]>)String[][].class);
	//	String[][] t = (String[][]) sh.bytes2object(by);
		long b = System.currentTimeMillis();
		for(int i=0;i<100000;i++) {
			JSONUtil.toObject(json, (Class<String[][]>)String[][].class);
	//		sh.bytes2object(by);
		}
		
		System.out.println(System.currentTimeMillis() - b);
	}
}
