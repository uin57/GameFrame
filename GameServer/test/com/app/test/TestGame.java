/**
 * 
 */
package com.app.test;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.app.AbstractTest;
import com.app.framework.context.ActionRequest;
import com.app.framework.context.Request;
import com.app.framework.http.HttpServiceRequest;
import com.app.framework.util.JSONUtil;
/**
 * @author lisong
 * @version 2013-2-27 下午3:03:15
 */
public class TestGame extends AbstractTest {

//	@Test
//	public void testService() throws Exception {
//		ServiceRequest req = new ServiceRequest();
//		Map<String,String> map = new HashMap<String,String>();
//		map.put("test2", "test2");
//		req.setMethodName("test");
//		req.setServiceName("test");
//		req.setParameters(map);
//		super.test(1,req);
//	}
//	
	@Test
	public void testService() throws Exception {
		Map<String,String> map = new HashMap<String,String>();
		map.put("test1","上份额");
		map.put("test2", "上份额");
		byte[] b = JSONUtil.toJSON(map).getBytes();
		System.out.println("上行数组length:"+b.length);
		HttpServiceRequest req = new HttpServiceRequest(b,(short)102,"test",null);
		super.test(req);
	}
	@Test
	public void testAction() throws Exception {
		Map<String,String> map = new HashMap<String,String>();
		map.put("test1", "我是中文1");
		map.put("test2", "我是中文2");
		ActionRequest req = new ActionRequest(JSONUtil.toJSON(map).getBytes(),(short)1,1,null);
		req.setRequestId(1);
		req.setSessionId("asdf");
		super.test(req);
	}
	
	@Test
	public void testSaveService() throws Exception {
		HttpServiceRequest req = new HttpServiceRequest(null,(short)101,"save",null);
		super.test(req);
	}
	
	@Test
	public void testResourceService() throws Exception {
		Map<String,String> param = new HashMap<String,String>();
		param.put("resver", "9");
		param.put("cver", "9");
		HttpServiceRequest req = new HttpServiceRequest(JSONUtil.toJSON(param).getBytes(),Request.SESSION_SERVICE_ID,"updateResource",null);
		super.test(req);
	}
//	@Test
//	public void testSaveService2() throws Exception {
//		ServiceRequest req = new ServiceRequest();
//		Map<String,String> map = new HashMap<String,String>();
//		req.setServiceName("save");
//		req.setMethodName("saveCascade");
//		req.setParameters(map);
//		long b = System.currentTimeMillis();
//		super.test(req);
//		System.out.println(System.currentTimeMillis() - b);
//	}
//	
//	@Test
//	public void testSaveServiceGet() throws Exception {
//		ServiceRequest req = new ServiceRequest();
//		Map<String,String> map = new HashMap<String,String>();
//		req.setServiceName("save");
//		req.setMethodName("getCascade");
//		req.setParameters(map);
//		super.test(req);
//	}
//	
//	@Test
//	public void testResource() throws Exception {
//		ServiceRequest req = new ServiceRequest();
//		Map<String,String> map = new HashMap<String,String>();
//		map.put("cver", "32");
//		map.put("resver", "32");
//		req.setServiceName("resource");
//		req.setMethodName("updateResource");
//		req.setParameters(map);
//		super.test(req);
//	}
//	@Test
//	public void testAddPlayerItem() throws Exception {
//		ServiceRequest req = new ServiceRequest();
//		Map<String,String> map = new HashMap<String,String>();
//		map.put("userUid", "aaaa");
//		req.setServiceName("playerItem");
//		req.setMethodName("add");
//		req.setParameters(map);
//		super.test(req);
//	}
//	
//	@Test
//	public void testRemovePlayerItem() throws Exception {
//		ServiceRequest req = new ServiceRequest();
//		Map<String,String> map = new HashMap<String,String>();
//		map.put("userUid", "aaaa");
//		map.put("itemUid", "fefd4be6-4e07-4249-91d7-52f18f4f398a");
//		req.setServiceName("playerItem");
//		req.setMethodName("remove");
//		req.setParameters(map);
//		super.test(req);
//	}
//	@Test
//	public void testRemovePlayerItemCollection() throws Exception {
//		ServiceRequest req = new ServiceRequest();
//		Map<String,String> map = new HashMap<String,String>();
//		map.put("userUid", "aaaa");
//		req.setServiceName("playerItem");
//		req.setMethodName("removeCollection");
//		req.setParameters(map);
//		super.test(req);
//	}
}
