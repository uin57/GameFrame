/**
 * 
 */
package com.app.gameserver.service.http;

import com.app.framework.config.PropConfigLoader;
import com.app.framework.http.HttpContext;
import com.app.framework.service.GameService;

/**
 * @author lisong
 * @version 2013-3-13 下午1:55:24
 */
public interface SessionService extends GameService {

	/**
	 * prop.xml中配置的session超时配置的id
	 */
	String SESSION_TIMEOUT_ID = "sessiontimeout";
	
	String CHECK_SESSION_ID = "checkSession";
	
	boolean CHECK_SESSION = Boolean.parseBoolean(PropConfigLoader.getInstance().getConfig(CHECK_SESSION_ID).getValue());
	
	int SESSION_TIMEOUT = Integer.parseInt(PropConfigLoader.getInstance().getConfig(SESSION_TIMEOUT_ID).getValue());
	
	String PARAM_USERUID = "userUid";
	/**
	 * memcache中sessionid的前缀
	 */
	String SESSION_KEY = "sessionId";
	
	/**
	 * 产生session后在本次下发数据时通过header下发
	 * @param userUid
	 * @param context
	 * @throws Exception
	 */
	public void createSession(long userId,HttpContext context) throws Exception;
	/**
	 * 检查session是否存在 检查是否重复提交
	 * @param context
	 * @throws Exception
	 */
	public void checkSession(HttpContext context) throws Exception;
	
}
