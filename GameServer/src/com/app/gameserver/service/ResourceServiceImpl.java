/**
 * 
 */
package com.app.gameserver.service;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.x6game.util.XStreamFactory;
import cn.x6game.util.helper.ResourceFile;
import cn.x6game.util.helper.ResourceXML;
import cn.x6game.util.helper.SubVerson;

import com.app.framework.annotations.ServiceClientVisible;
import com.app.framework.config.PropConfigLoader;
import com.app.framework.context.AbstractContext;
import com.app.framework.result.RequestResult;
import com.app.framework.util.ErrorCodeConstants;
import com.app.framework.util.JSONUtil;
import com.app.framework.util.StringUtils;
import com.app.gameserver.service.model.Resource;

/**
 * @author lisong
 * @version 2013-4-12 下午5:36:50
 */
public class ResourceServiceImpl implements ResourceService {

	private static ResourceXML resource;
	private static String resourceURL;
	static {
		try {
			resource = (ResourceXML) XStreamFactory.createXStream().fromXML(new File(System.getProperty("user.dir")+"/resource/compare/rescompare.xml"));
		}catch (Exception e) {
			System.out.println("未成功读取资源xml文件:"+ e.getMessage());
		}
		
		 resourceURL = PropConfigLoader.getInstance().getConfig("resourceURL").getValue();
	}
	@SuppressWarnings("unchecked")
	@ServiceClientVisible
	@Override
	public RequestResult<Resource> updateResource(AbstractContext context)
			throws Exception {
		Map<String,Object> param = JSONUtil.toMap(context.getContent());
		
		int clientResVer = Integer.parseInt(param.get(RESOURCE_VERSON_PARAM_STRING).toString());
		
		float clientVer = Float.parseFloat(param.get(CLIENT_VERSON_PARAM_STRING).toString());
		
		RequestResult<Resource> rs = checkResource(clientResVer, clientVer);
		
		return rs;
	}
	
	@SuppressWarnings({ "unchecked"})
	public RequestResult<Resource> checkResource(int clientResVer,float clientVer) {
		if(resource == null) {
			return RequestResult.fail(ErrorCodeConstants.SERVER_ERROR_RESCOMP_NOT_EXIST_OR_NO_DATA, "rescompare.xml not exist or no data");
		}
		if(resource.getVerson() <= clientResVer) {
			return RequestResult.success(null);
		}
		if(!resource.validateVer(clientResVer)) {
			//需要重新下载版本
			return RequestResult.fail(ErrorCodeConstants.CLIENT_VER_TOO_LOW, "client version is too low");
		}
		//从最低版本开始放入		
		//key add或者remove 
		//value key file
		List<ResourceFile> res = new ArrayList<ResourceFile>();

		for(int i=resource.getSubverson().size() - 1;i>=0;i--) {
			SubVerson sv = resource.getSubverson().get(i);
			if(sv.getVerson() <= clientResVer) {//只遍历比客户端资源版本高的记录
				continue;
			}
			//值下发允许当前客户端版本更新的资源
			if(!StringUtils.isNullOrEmpty(sv.getClientVerson())) {
				float tmp = Float.parseFloat(sv.getClientVerson());//允许的最低客户端版本
				if(clientVer < tmp) {
					break;//因为从低开始向高版本遍历 因此认为 若当前客户端版本低于该资源版本标记的最低客户端版本,则认为更高资源版本标记的最低客户端版本与该资源版本标记的相同或更高
				}
			}
			for(ResourceFile rf : sv.getResourceFile()) {
				res.add(rf);//添加到下发列表中
			}
		}
		if(res.size() == 0) {
			return RequestResult.success(null);
		}
		Resource rs = new Resource();
		rs.setResURL(resourceURL);
		rs.setResVer(resource.getVerson());
		rs.setResXml(res);
		Map<String,Object> ret = new HashMap<String,Object>();
		ret.put("resURL", resourceURL);
		ret.put("resVer", resource.getVerson());
		ret.put("resXml", res);
		return RequestResult.success(rs);
	}
}
