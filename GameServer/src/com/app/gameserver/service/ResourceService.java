/**
 * 
 */
package com.app.gameserver.service;

import com.app.framework.context.AbstractContext;
import com.app.framework.result.RequestResult;
import com.app.framework.service.GameService;
import com.app.gameserver.service.model.Resource;

/**
 * @author lisong
 * @version 2013-4-12 下午5:36:03
 */
public interface ResourceService extends GameService {

	/**
	 * 客户端版本号参数名
	 */
	String CLIENT_VERSON_PARAM_STRING = "cver";
	/**
	 * 客户端资源版本号参数名
	 */
	String RESOURCE_VERSON_PARAM_STRING = "resver";
	
	public RequestResult<Resource> updateResource(AbstractContext context) throws Exception;
}
