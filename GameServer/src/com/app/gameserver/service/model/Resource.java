package com.app.gameserver.service.model;

import java.util.List;

import cn.x6game.util.helper.ResourceFile;

public class Resource {

	private String resURL;
	
	private int resVer;
	
	private List<ResourceFile> resXml;

	public String getResURL() {
		return resURL;
	}

	public void setResURL(String resURL) {
		this.resURL = resURL;
	}

	public int getResVer() {
		return resVer;
	}

	public void setResVer(int resVer) {
		this.resVer = resVer;
	}

	public List<ResourceFile> getResXml() {
		return resXml;
	}

	public void setResXml(List<ResourceFile> resXml) {
		this.resXml = resXml;
	}
}
