/**
 * 
 */
package com.app.service.user;

import com.app.framework.orm.ActiveRecord;

/**
 * @author lisong
 * @version 2013-4-25 下午5:36:30
 */
public abstract class AbstractUser extends ActiveRecord implements User {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8732958114671940725L;

}
