/**
 * 
 */
package com.app.service.user;

import com.app.framework.orm.StorableModel;
import com.app.framework.result.Result;

/**
 * @author lisong
 * @version 2013-3-6 上午11:12:53
 */
public interface User extends StorableModel, Result {

}
