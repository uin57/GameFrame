/**
 * 
 */
package com.app.service.item;

import com.app.framework.orm.CollectionItem;

/**
 * @author lisong
 * @version 2013-3-6 下午3:22:01
 */
public class UserItem extends CollectionItem {

	/**
	 * 
	 */
	private static final long serialVersionUID = 461924982139359107L;

	/**
	 * 对应设定数据的id
	 */
	private String itemId;
	
	private long userId;
	
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
}
