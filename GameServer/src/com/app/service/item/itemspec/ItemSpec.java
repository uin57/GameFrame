/**
 * 
 */
package com.app.service.item.itemspec;

/**
 * @author lisong
 * @version 2013-3-29 下午5:04:01
 */
public class ItemSpec implements ItemFace {

	protected String id;
	protected String name;
	protected String description;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
