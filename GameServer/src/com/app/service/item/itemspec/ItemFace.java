/**
 * 
 */
package com.app.service.item.itemspec;


/**
 * @author lisong
 * @version 2013-4-2 上午11:27:38
 */
public interface ItemFace {

	/**
	 * 获得该物品ID
	 * @return String
	 */
	String getId();
	/**
	 * 设置该物品ID
	 */
	void setId(String id);	
	/**
	 * 获得该物品名
	 * @return String
	 */	
	String getName();
	/**
	 * 设置该物品名
	 */
	void setName(String name);
	
	String getDescription();
	
	void setDescription(String desc);
	
}
