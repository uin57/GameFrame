package com.app.worldserver.action;

import com.app.framework.socket.SocketContext;
import com.app.framework.socket.manager.ServerManager;
import com.app.framework.socket.msg.SystemProtoBuffer;
import com.app.framework.socket.msg.SystemProtoBuffer.Register;
import com.app.framework.socket.sender.Server;
import com.app.service.action.SocketAction;

public class RegisterAction extends SocketAction {

	@Override
	public void execute(SocketContext context) throws Exception {
		Register register = SystemProtoBuffer.Register.parseFrom(context.getContent());
		Server s = (Server) context.getResponse().getSender();
		s.setId(register.getServerId());
		s.setType(register.getServerType());
		ServerManager.getInstance().addSender(s);
		logger.warn("register:"+s.getChannel());
	}

}
