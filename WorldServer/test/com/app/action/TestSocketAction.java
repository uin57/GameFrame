/**
 * 
 */
package com.app.action;

import java.util.concurrent.atomic.AtomicLong;

import com.app.framework.socket.Packet;
import com.app.framework.socket.SocketContext;
import com.app.service.action.SocketAction;

/**
 * @author lisong
 * @version 2013-3-8 下午3:36:11
 */
public class TestSocketAction extends SocketAction {

	static final AtomicLong ato = new AtomicLong();
	public void execute(SocketContext context) throws Exception {
		String str = new String(context.getContent());
		if(ato.incrementAndGet() % 10000 == 0) {
			logger.warn("world 收到 server 消息 并且返回给server:"+ato.get());
		}
		Packet p = new Packet((short)10000, ("world已收到消息"+str).getBytes("UTF-8"));
		context.sendResponse(p);
	}
}
