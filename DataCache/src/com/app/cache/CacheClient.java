package com.app.cache;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.app.exception.DCacheException;

public interface CacheClient {


	String CACHE_RECORD_NULL_VALUE = "$NULL$";
	/**
	 * 将一个或多个 value 元素加入到set集合 key 当中，已经存在于集合的 member 元素将被忽略。
	 * 假如 key 不存在，则创建一个只包含 member 元素作成员的集合。
	 * 当 key 不是集合类型时，返回一个错误。
	 * @param key
	 * @param value
	 * @return
	 * @throws DCacheException
	 */
	long sadd(String key, String... value) throws DCacheException;
	
	/**
	 * set集合的大小
	 * @param key
	 * @return 当 key 不存在时，返回 0
	 * @throws DCacheException
	 */
	long slength(String key);
	
	/**
	 * 移除并返回set集合中的一个随机元素
	 * @param key
	 * @return 当 key 不存在或 key 是空集时，返回null
	 * @throws DCacheException
	 */
	String spop(String key);
	
	/**
	 * 为key的set集合中是否包含value值
	 * @param key
	 * @return
	 * @throws DCacheException
	 */
	boolean scontains(String key,String value);
	
	/**
	 * 随机返回set集合中的一个元素
	 * @param key
	 * @return 如果set集合为空或不存在 返回null
	 * @throws DCacheException
	 */
	String sget(String key);
	
	/**
	 * 移除set集合中的value
	 * @param key
	 * @param value
	 * @return 被成功移除的元素的数量 当 key 不是集合类型，返回一个错误
	 * @throws DCacheException
	 */
	long sremove(String key,String... value) throws DCacheException;
	
	/**
	 * 返回集合中的所有元素
	 * @param key
	 * @return
	 */
	Set<String> svalues(String key);
	
	/**
	 * 移除 key
	 * @param key
	 * @return 被删除 key 的数量 
	 *  删除单个字符串类型的 key ，时间复杂度为O(1)。
		删除单个列表、集合、有序集合或哈希表类型的 key ，时间复杂度为O(M)， M 为以上数据结构内的元素数量
	 */
	long del(String... key);
	
	/**
	 * key是否存在
	 * @param key
	 * @return
	 */
	boolean exists(String key);
	
	/**
	 * 设置key的存在时间
	 * 可以对一个已经带有生存时间的 key 执行 setTimeoutOfKey 命令，新指定的生存时间会取代旧的生存时间
	 * 注意:使用不带时间的set或getSet命令后,生存时间将会不存在,即key不再有过期时间
	 * @param key
	 * @param seconds 秒
	 * @return 当 key 不存在或者不能为 key 设置生存时间时 返回false
	 */
	boolean setExpire(String key,int seconds);
	
	/**
	 * 移除key的超时时间,即 永不超时
	 * @param key
	 * @return
	 */
	boolean removeExpire(String key);
	
	/**
	 * 返回 key 所储存的值的类型
	 * @param key
	 * @return
	 * 		none (key不存在)
			string (字符串)
			list (列表)
			set (集合)
			zset (有序集)
			hash (哈希表)
	 */
	String getTypeOfKey(String key);
	
	/**
	 * 
	 * @param key
	 * @return 
	 * 			如果值包含错误的类型，或字符串类型的值不能表示为数字，那么返回一个错误
	 */
	long decrement(String key) throws DCacheException;
	
	long decrement(String key ,long num) throws DCacheException;
	/**
	 * 
	 * @param key
	 * @return 如果值包含错误的类型，或字符串类型的值不能表示为数字，那么返回一个错误
	 */
	long increment(String key) throws DCacheException;
	
	long increment(String key ,long num) throws DCacheException;
	
	/**
	 * 
	 * @param key
	 * @return
	 * @throws DCacheException 假如 key 储存的值不是字符串类型，返回一个错误，因为 GET 只能用于处理字符
	 */
	String get(String key) throws DCacheException;
	
	/**
	 * 返回 key对应的value字符串
	 * @param key...
	 * @return 如果给定的 key 里面，有某个 key 不存在，则对应的列表index为null。因此，该命令永不失败。
	 */
	List<String> mgets(String... keys);
	
	boolean set(String key,String value);
	
	boolean set(String key,String value,int second);
	/**
	 * 批量设置key-value对
	 * @param kv
	 */
	void mset(String... kv);
	
	/**
	 * 仅当key不存在时返回true
	 * @param key
	 * @param value
	 * @param second
	 * @return
	 */
	boolean setIfAbsent(String key,String value,int second);
	
	boolean setIfAbsent(String key,String value);
	
	/**
	 * 删除哈希表 key 中的一个或多个指定域，不存在的域将被忽略
	 * @param key 存储条目的key
	 * @param keys map的key
	 * @return 被成功移除的域的数量，不包括被忽略的域
	 */
	long hdel(String key,String... keys) throws DCacheException;
	
	
	boolean hcontainsKey(String key,String k) throws DCacheException;
	
	/**
	 * 获取map的所有的项
	 * @param key
	 * @return
	 * @throws DCacheException
	 */
	Map<String,String> hgetAll(String key) throws DCacheException;

	/**
	 * 
	 * @param key
	 * @param k
	 * @param num 负数则为减
	 * @return 如果 key 不存在，一个新的哈希表被创建并执行 HINCRBY 命令。如果域 field 不存在，那么在执行命令前，域的值被初始化为 0
	 * @throws DCacheException
	 */
	long hincrement(String key,String k,long num) throws DCacheException;
	
	/**
	 *  key 不存在时，返回一个空表。
	 * @param key
	 * @return
	 */
	Set<String> hkeys(String key);
	/**
	 *  key 不存在时，返回一个空表。
	 * @param key
	 * @return
	 */
	List<String> hvalues(String key);
	
	/**
	 * 
	 * @param key
	 * @return 当 key 不存在时，返回 0 
	 */
	long hlength(String key);
	
	boolean hmset(String key,Map<String,String> map) throws DCacheException;
	
	void hset(String key,String k,String v);
	
	/**
	 *如果 key 不存在，一个新哈希表被创建并执行 HSETNX 命令
	 * @param key
	 * @param k
	 * @param v
	 * @return
	 */
	boolean hsetnx(String key,String k,String v);
	
	/**
	 * 阻塞式检出头元素
	 * @param key
	 * @return key 列表名 value 列表的项
	 * @throws DCacheException
	 */
	Map<String,String> lbpop(String... key) throws DCacheException;
	
	/**
	 * 
	 * @param timeout
	 * @param key 
	 * @return key 列表名 value 列表的项
	 * @throws DCacheException
	 */
	Map<String,String> lbpop(int timeout,String... key) throws DCacheException;
	
	/**
	 * 非阻塞检出头元素
	 * @param key
	 * @return 可为null
	 * @throws DCacheException
	 */
	String lpop(String key) throws DCacheException;
	
	long llength(String key) throws DCacheException;
	
	/**
	 * 像arraylist那样放入数据
	 * @param key
	 * @param values
	 * @return 长度
	 * @throws DCacheException
	 */
	long lpush(String key,String... values) throws DCacheException;
	/**
	 * 仅当key存在时才产生操作
	 * @param key
	 * @param values
	 * @return
	 * @throws DCacheException
	 */
	long lrpushex(String key,String... values) throws DCacheException;
	
}
