package com.app.cache;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import com.app.config.DCacheConfig;
import com.app.exception.DCacheException;

public class RedisClient implements CacheClient {

	private final JedisPool pool;
	RedisClient() {
		JedisPoolConfig config = new JedisPoolConfig();
		DCacheConfig cfg = DCacheConfig.getInstance();
		config.setMaxTotal(cfg.getPoolMaxActive());
		config.setMaxIdle(cfg.getPoolMaxIdle());
		config.setMaxWaitMillis(cfg.getPoolMaxWait());
		config.setTestOnBorrow(true);
		config.setTestOnReturn(true);
		config.setBlockWhenExhausted(true);//则表示阻塞住，或者达到maxWait时抛出JedisConnectionException
		pool = new JedisPool(config, cfg.getCacheIp(), cfg.getCachePort(),cfg.getPoolMaxWait());//第四个参数 socket的读取 超时时间
	}
	
	public Jedis getJedis() {
		return pool.getResource();
	}
	
	public void returnJedis(Jedis jedis) {
		if(jedis != null) {
			pool.returnResource(jedis);
		}
	}
	
	@Override
	public long sadd(String key, String... value) throws DCacheException {
		long result = 0;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.sadd(key, value);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return result;
	}
	
	@Override
	public long slength(String key) throws DCacheException {
		long result = 0;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.scard(key);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return result;
	}
	
	@Override
	public boolean scontains(String key, String value) throws DCacheException {
		boolean result = false;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.sismember(key, value);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return result;
	}

	@Override
	public String spop(String key) {
		String result = null;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.spop(key);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return result;
	}

	@Override
	public String sget(String key) {
		String result = null;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.srandmember(key);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return result;
	}

	@Override
	public long sremove(String key, String... value) throws DCacheException {
		long result = 0;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.srem(key, value);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return result;
	}

	@Override
	public Set<String> svalues(String key) {
		Set<String> result = null;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.smembers(key);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return result;
	}
	@Override
	public long del(String... key) {
		long result = 0;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.del(key);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return result;
	}
	
	@Override
	public List<String> mgets(String... keys) {
		List<String> ret = null;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			ret = jedis.mget(keys);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return ret;
	}

	@Override
	public boolean exists(String key) {
		boolean result = false;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.exists(key);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return result;
	}

	@Override
	public boolean setExpire(String key, int seconds) {
		long result = 0;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.expire(key, seconds);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return result == 1;
	}

	@Override
	public boolean removeExpire(String key) {
		long result = 0;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.persist(key);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return result == 1;
	}

	@Override
	public String getTypeOfKey(String key) {
		String result = null;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.type(key);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return result;
	}

	@Override
	public long decrement(String key) throws DCacheException {
		long result = 0;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.decr(key);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return result;
	}

	@Override
	public long decrement(String key, long num) throws DCacheException {
		long result = 0;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.decrBy(key,num);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return result;
	}

	@Override
	public long increment(String key) throws DCacheException {
		long result = 0;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.incr(key);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return result;
	}

	@Override
	public long increment(String key, long num) throws DCacheException {
		long result = 0;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.incrBy(key,num);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return result;
	}

	@Override
	public String get(String key) throws DCacheException {
		String result = null;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.get(key);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return result;
	}

	@Override
	public boolean set(String key, String value) {
		String result = null;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.set(key, value);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return  result!= null && result.indexOf("OK") != -1;
	}
	
	@Override
	public boolean set(String key, String value, int second) {
		String result = null;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.setex(key, second, value);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return  result!= null && result.indexOf("OK") != -1;
//		boolean ret = set(key, value);
//		if(!setExpire(key, second)) {
//			System.out.println("失败");
//		}
//		return ret;
	}

	@Override
	public void mset(String... kv) {
		Jedis jedis = null;
		try {
			jedis = getJedis();
			jedis.mset(kv);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
	}

	@Override
	public boolean setIfAbsent(String key,String value,int second) {
		String result = null;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.set(key, value, "NX", "EX", second);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return result!= null && result.indexOf("OK") != -1;
	}
	
	@Override
	public boolean setIfAbsent(String key, String value) {
		long result = 0;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.setnx(key, value);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return result == 1l;
	}

	@Override
	public long hdel(String key, String... keys) throws DCacheException {
		long result = 0;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.hdel(key, keys);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return result;
	}
	
	@Override
	public boolean hcontainsKey(String key, String k) throws DCacheException {
		boolean result = false;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.hexists(key, k);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return result;
	}
	
	@Override
	public Map<String, String> hgetAll(String key) throws DCacheException {
		Map<String, String> result = null;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.hgetAll(key);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return result;
	}
	
	@Override
	public long hincrement(String key, String k, long num)
			throws DCacheException {
		long result = 0;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.hincrBy(key, k, num);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return result;
	}
	
	@Override
	public Set<String> hkeys(String key) {
		Set<String> result = null;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.hkeys(key);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return result;
	}
	
	@Override
	public List<String> hvalues(String key) {
		List<String> result = null;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.hvals(key);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return result;
	}
	
	@Override
	public long hlength(String key) {
		long result = 0;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.hlen(key);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return result;
	}
	
	@Override
	public boolean hmset(String key, Map<String, String> map)
			throws DCacheException {
		String result = null;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.hmset(key, map);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return  result!= null && result.indexOf("OK") != -1;
	}
	
	@Override
	public void hset(String key, String k, String v) {
		Jedis jedis = null;
		try {
			jedis = getJedis();
			jedis.hset(key, k, v);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
	}
	
	@Override
	public boolean hsetnx(String key, String k, String v) {
		long ret = 0;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			ret = jedis.hsetnx(key, k, v);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return ret == 1;
	}
	
	@Override
	public Map<String,String> lbpop(int timeout, String... key) throws DCacheException {
		Map<String,String> ret = new HashMap<String, String>();
		Jedis jedis = null;
		try {
			jedis = getJedis();
			List<String> lst = jedis.blpop(timeout, key);
			for(int i=0;i<lst.size();i+=2) {
				ret.put(lst.get(i), lst.get(i+1));
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return ret;
	}
	
	@Override
	public Map<String,String> lbpop(String... key) throws DCacheException {
		Map<String,String> ret = new HashMap<String, String>();
		Jedis jedis = null;
		try {
			jedis = getJedis();
			List<String> lst = jedis.blpop(key);
			for(int i=0;i<lst.size();i+=2) {
				ret.put(lst.get(i), lst.get(i+1));
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return ret;
	}
	
	@Override
	public long llength(String key) throws DCacheException {
		long result = 0;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.llen(key);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return result;
	}
	
	@Override
	public String lpop(String key) throws DCacheException {
		String result = null;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.lpop(key);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return result;
	}
	
	@Override
	public long lpush(String key, String... values) throws DCacheException {
		long result = 0;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.rpush(key, values);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return result;
	}
	
	@Override
	public long lrpushex(String key, String... values) throws DCacheException {
		long result = 0;
		Jedis jedis = null;
		try {
			jedis = getJedis();
			result = jedis.rpushx(key, values);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} finally {
			returnJedis(jedis);
		}
		return result;
	}
	
	public static void main(String[] args) throws Exception {
//		final RedisClient rc = new RedisClient();
//		for(int i=0;i<700;i++) {
//			Thread t = new Thread(new Runnable() {
//				
//				@Override
//				public void run() {
//					for(int i=0;i<100;i++) {
//						rc.set("aaa", "aaa", 1000);
//					}
//				}
//			});
//			t.start();
//			t.join();
//		}
	}
}
