package com.app.cache;

public class CacheFactory {

	private static final RedisClient redis = new RedisClient();
	
//	private static final MemClient mem = new MemClient();
	private CacheFactory(){}
	public static RedisClient getRedisClient() {
		
		return redis;
	}
	
//	public static MemClient getMemClient() {
//		
//		return mem;
//	}
}
