/**
 * 
 */
package com.app.cache;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.rubyeye.xmemcached.MemcachedClient;
import net.rubyeye.xmemcached.MemcachedClientBuilder;
import net.rubyeye.xmemcached.XMemcachedClientBuilder;
import net.rubyeye.xmemcached.command.BinaryCommandFactory;
import net.rubyeye.xmemcached.utils.AddrUtil;

import org.apache.log4j.Logger;

import com.app.config.DCacheConfig;
import com.app.exception.DCacheException;

/**
 * @author lisong
 * @version 2013-5-24 上午10:00:22
 */
public class MemClient implements CacheClient {

	private static MemcachedClient client;
	private static final Logger logger = Logger.getLogger(MemClient.class); 
	MemClient() {
		try {
			MemcachedClientBuilder builder = new XMemcachedClientBuilder(AddrUtil.getAddresses(DCacheConfig.getInstance().getCacheIp()+":"+DCacheConfig.getInstance().getCachePort()));
			builder.setConnectionPoolSize(5);//设置连接池大小
			builder.setCommandFactory(new BinaryCommandFactory());
			builder.setOpTimeout(10000);
			builder.setConnectTimeout(10000);
			client = builder.build();
			client.getTranscoder().setPrimitiveAsString(true);
		}catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			throw new DCacheException("memcache 初始化失败");
		}
	}
	/**
	 * 
	 * @param memKey
	 * @param value
	 * @param expire 单位秒
	 * @throws DCacheException 
	 */
	@Override
	public boolean set(String memKey,String value,int expire) throws DCacheException {
		try {
			return client.set(memKey, expire, value);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			throw new DCacheException(e);
		} 
	}
	/**
	 * 
	 * @param memKey
	 * @param value
	 * @return
	 * @throws DCacheException
	 */
	@Override
	public boolean set(String memKey,String value) throws DCacheException {
		try {
			return client.set(memKey, 0, value);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			throw new DCacheException(e);
		} 
	}
	/**
	 * 
	 * @param memKey
	 * @param value
	 * @param expire 单位 秒
	 * @return
	 * @throws DCacheException
	 */
	@Override
	public boolean setIfAbsent(String memKey,String value,int expire) throws DCacheException {
		try {
			return client.add(memKey, expire, value);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			throw new DCacheException(e);
		} 
	}
	
	@Override
	public boolean setIfAbsent(String memKey, String value) {
		return setIfAbsent(memKey, value, 0);
	}
	
	/**
	 * 删除数据
	 * @param memKey
	 * @return
	 * @throws DCacheException
	 */
	public boolean del(String memKey) throws DCacheException {
		try {
			return client.delete(memKey);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			throw new DCacheException(e);
		} 
	}
	/**
	 * 递减
	 * @param memKey
	 * @param num
	 * @param init 当key值不存在时的初始值
	 * @return
	 * @throws DCacheException
	 */
	public long decr(String memKey,long num,long init) throws DCacheException {
		try {
			return client.decr(memKey, num, init);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			throw new DCacheException(e);
		} 
	}
	/**
	 * 递增
	 * @param memKey
	 * @param num
	 * @param init当key值不存在时的初始值
	 * @return
	 * @throws DCacheException
	 */
	public long incr(String memKey,long num,long init) throws DCacheException {
		try {
			return client.incr(memKey, num, init);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			throw new DCacheException(e);
		} 
	}
	/**
	 * 更新数据超时时间
	 * @param memKey
	 * @param exp 新的超时时间 单位秒
	 * @return
	 * @throws DCacheException
	 */
	public boolean touch(String memKey,int exp) throws DCacheException {
		try {
			return client.touch(memKey, exp);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			throw new DCacheException(e);
		} 
	}
	/**
	 * 获取数据并且设置新的超时时间
	 * @param memKey
	 * @param exp新的超时时间
	 * @return
	 * @throws DCacheException
	 */
	public String getAndTouch(String memKey,int exp) throws DCacheException {
		try {
			return client.getAndTouch(memKey, exp);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			throw new DCacheException(e);
		} 
	}
	
	@Override
	public String get(String memKey) throws DCacheException {
		try {
			return client.get(memKey);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			throw new DCacheException(e);
		} 
	}
	/**
	 * 批量获取数据
	 * @param keys
	 * @return
	 * @throws DCacheException
	 */
	@Override
	public List<String> mgets(String... keys) throws DCacheException {
		try {
			List<String> lst = Arrays.asList(keys);
			Map<String,String> map = client.get(lst);
			return new ArrayList<String>(map.values());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			throw new DCacheException(e);
		} 
	}
	
	public Map<InetSocketAddress, Map<String, String>> getStats() throws DCacheException {
		try {
			return client.getStats();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			throw new DCacheException(e);
		} 
	}
	@Override
	public long sadd(String key, String... value) throws DCacheException {
		throw new UnsupportedOperationException();
	}
	@Override
	public long slength(String key) {
		throw new UnsupportedOperationException();
	}
	@Override
	public String spop(String key) {
		throw new UnsupportedOperationException();
	}
	@Override
	public boolean scontains(String key, String value) {
		throw new UnsupportedOperationException();
	}
	@Override
	public String sget(String key) {
		throw new UnsupportedOperationException();
	}
	@Override
	public long sremove(String key, String... value) throws DCacheException {
		throw new UnsupportedOperationException();
	}
	@Override
	public Set<String> svalues(String key) {
		throw new UnsupportedOperationException();
	}
	@Override
	public long del(String... key) {
		throw new UnsupportedOperationException();
	}
	@Override
	public boolean exists(String key) {
		throw new UnsupportedOperationException();
	}
	@Override
	public boolean setExpire(String key, int seconds) {
		throw new UnsupportedOperationException();
	}
	@Override
	public boolean removeExpire(String key) {
		throw new UnsupportedOperationException();
	}
	@Override
	public String getTypeOfKey(String key) {
		throw new UnsupportedOperationException();
	}
	@Override
	public long decrement(String key) throws DCacheException {
		try {
			return client.decr(key, 1);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} 
			
	}
	@Override
	public long decrement(String key, long num) throws DCacheException {
		try {
			return client.decr(key, num);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} 
	}
	@Override
	public long increment(String key) throws DCacheException {
		try {
			return client.incr(key, 1);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} 
	}
	@Override
	public long increment(String key, long num) throws DCacheException {
		try {
			return client.incr(key, num);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		} 
	}
	@Override
	public void mset(String... kv) {
		throw new UnsupportedOperationException();
	}
	@Override
	public long hdel(String key, String... keys) throws DCacheException {
		throw new UnsupportedOperationException();
	}
	@Override
	public boolean hcontainsKey(String key, String k) throws DCacheException {
		throw new UnsupportedOperationException();
	}
	@Override
	public Map<String, String> hgetAll(String key) throws DCacheException {
		throw new UnsupportedOperationException();
	}
	@Override
	public long hincrement(String key, String k, long num)
			throws DCacheException {
		throw new UnsupportedOperationException();
	}
	@Override
	public Set<String> hkeys(String key) {
		throw new UnsupportedOperationException();
	}
	@Override
	public List<String> hvalues(String key) {
		throw new UnsupportedOperationException();
	}
	@Override
	public long hlength(String key) {
		throw new UnsupportedOperationException();
	}
	@Override
	public boolean hmset(String key, Map<String, String> map)
			throws DCacheException {
		throw new UnsupportedOperationException();
	}
	@Override
	public void hset(String key, String k, String v) {
		throw new UnsupportedOperationException();
		
	}
	@Override
	public boolean hsetnx(String key, String k, String v) {
		throw new UnsupportedOperationException();
	}
	@Override
	public Map<String, String> lbpop(String... key) throws DCacheException {
		throw new UnsupportedOperationException();
	}
	@Override
	public Map<String, String> lbpop(int timeout, String... key)
			throws DCacheException {
		throw new UnsupportedOperationException();
	}
	@Override
	public String lpop(String key) throws DCacheException {
		throw new UnsupportedOperationException();
	}
	@Override
	public long llength(String key) throws DCacheException {
		throw new UnsupportedOperationException();
	}
	@Override
	public long lpush(String key, String... values) throws DCacheException {
		throw new UnsupportedOperationException();
	}
	@Override
	public long lrpushex(String key, String... values) throws DCacheException {
		throw new UnsupportedOperationException();
	}
}
