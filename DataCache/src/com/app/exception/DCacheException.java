/**
 * 
 */
package com.app.exception;

/**
 * @author lisong
 * @version 2012-7-4 下午3:21:03
 */
public class DCacheException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DCacheException(String message) {
		super(message);
	}
	
	public DCacheException(String message,Throwable throwable) {
		super(message,throwable);
	}

	public DCacheException(Throwable throwable) {
		super(throwable);
	}
}
