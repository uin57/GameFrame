package com.app.store;


public class StoreFactory {

	private static KeyValueStore keyValueStore = new KeyValueStore();
	
	private static ColumnStore columnStore = new ColumnStore();
	
	private StoreFactory() {}
	
	public static KeyValueStore newKeyValueStore() {
		return keyValueStore;
	}
	
	public static ColumnStore newColumnStore() {
		return columnStore;
	}
	
	public static void main(String[] args) {
	}
}
