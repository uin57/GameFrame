package com.app.store;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.app.config.DCacheConfig;
import com.app.exception.DCacheException;
import com.app.model.Condition;
import com.app.model.IndexEntry;

public interface Store {
	
	int KV_STORE = 1;
	
	int COLUM_STORE = 2;
	//换算成秒
	int DEFAULT_EXPIRE = DCacheConfig.getInstance().getCacheExpire() * 24 * 60 * 60;

	int getStoryType();
	
	void delEntity(String tableName, long id,boolean flushdb) throws DCacheException;

    void delEntityList(String tableName, List<Long> keyList,boolean flushdb) throws DCacheException;

    String getEntity(String tableName, long id) throws DCacheException;

    Map<Long, String> getEntityList(String tableName, List<Long> keyList) throws DCacheException;

    /**
     * 
     * @param tableName
     * @param id
     * @param value
     * @param indexAttr
     * @param indexValue
     * @param indexType
     * @param changed 改变的 字段名 字段值
     * @return
     * @throws DCacheException
     */
    int putEntity(String tableName, long id, String value, List<String> indexAttr, List<String> indexValue,
    		LinkedHashMap<String,String> changed,boolean flushdb) throws DCacheException;

    /**
     * 
     * @param tableName
     * @param keyList
     * @param valueList
     * @param indexAttr
     * @param indexValueList
     * @param indexType
     * @param changedList list的项: 改变的 字段名 字段值
     * @throws DCacheException
     */
    void putEntityList(String tableName, List<Long> keyList, List<String> valueList, List<String> indexAttr,
            List<List<String>> indexValueList,List<LinkedHashMap<String,String>> changedList,boolean flushdb) throws DCacheException;
    
    void putMultiEntityList(List<String> tableNameList, List<Long> keyList, List<String> valueList,  
    		List<List<String>> indexAttrList, List<List<String>> indexValueList,List<LinkedHashMap<String,String>> changedList,boolean flushdb) throws DCacheException;

    List<String> queryEntityList(String tableName, Condition condition) throws DCacheException;
    
    List<IndexEntry> queryIndexList(String tableName, Condition condition) throws DCacheException;

    long countEntities(String tableName) throws DCacheException;
    
    long countWithCondition(String tableName, Condition condition) throws DCacheException;
    
    /**
     * 
     * @param key
     * @param num
     * @param expire 单位 秒
     * @return
     * @throws DCacheException
     */
    long increment(String key,int num,int expire) throws DCacheException;
    
    long increment(String key,int num) throws DCacheException;
    
    long increment(String key) throws DCacheException;
    /**
     * 
     * @param key
     * @param num
     * @param expire 单位 秒
     * @return
     * @throws DCacheException
     */
    long decrement(String key,int num,int expire) throws DCacheException;
    
    long decrement(String key,int num) throws DCacheException;
    
    long decrement(String key) throws DCacheException;
    
    long delOnlyCache(String key) throws DCacheException;
    /**
     * 只放入缓存,永不过期
     * @param key
     * @param value
     * @param expire 单位 秒
     * @throws DCacheException
     */
    void putOnlyCache(String key,String value,int expire) throws DCacheException;
    /**
     * 如果cache中不存在,则放入
     * 永不过期
     * @param key
     * @param value
     * @throws DCacheException
     */
    void putOnlyCacheIfAbsent(String key,String value) throws DCacheException;
    
    String getOnlyCache(String key) throws DCacheException;
    
    boolean lockRecord(String key) throws DCacheException;
    
    boolean releaseRecord(String key) throws DCacheException;
    
    long getMaxEntityId(String tableName) throws DCacheException;
    
}
