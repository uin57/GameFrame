/**
 * 
 */
package com.app.config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Properties;

import com.app.exception.DCacheException;
import com.app.util.NumberUtils;

/**
 * @author lisong
 * @version 2012-7-5 下午4:02:25
 */
public class DCacheConfig {

	private String databaseIP;
	private int databasePort;
	private String databaseUser;
	private String databaseName;
	private String databasePassword;
	private int databaseInitConn;
	private int databaseMaxConn;
	private int partitionCount;//默认为4
	private String cacheIp;//ip
	private int cachePort;
	private String preloadPwd;
	private String tableScanBasePakage;
	private int tablePartition;
	private int poolMaxActive;
	private int poolMaxWait;
	private int poolMaxIdle;
	private int updateInterval;//数据更新间隔 单位秒
	private int updateThreadNum;
	private int tableStartNum;//表主键的起始值
	private int cacheExpire;//数据过期时间 ,单位天
	/**
	 * 默认分片
	 */
	private static final int DEFAULT_PARTITIONCOUNT = 4;
	
	private static DCacheConfig instance;
	private DCacheConfig() {
		try {
			load();
		} catch (Exception e) {
			e.printStackTrace();
			throw new DCacheException(e);
		}
	}
	
	public static DCacheConfig getInstance() {
		if(instance == null) {
			instance = new DCacheConfig();
		}
		return instance;
	}
	
	public void load() throws Exception {
		Reader reader = null;
		//允许配置文件放在clsspath目录和运行jar相同的目录下,且运行jar目录下优先
		try{
			reader = new InputStreamReader(new FileInputStream(System.getProperty("user.dir")+"/dcache.properties"),"UTF-8");
		}catch (FileNotFoundException e) {
			reader = new InputStreamReader(this.getClass().getResourceAsStream("/dcache.properties"),"UTF-8");
		}
			
    	Properties prop=new Properties();
    	prop.load(reader);
    	reader.close();
    	databaseIP = prop.getProperty("dbhost");
    	databasePort = Integer.parseInt(prop.getProperty("dbport"));
    	databaseUser = prop.getProperty("dbuser");
    	databaseName = prop.getProperty("dbname");
    	databasePassword = prop.getProperty("dbpwd");
    	databaseInitConn = Integer.parseInt(prop.getProperty("dbinitconn"));
    	databaseMaxConn = Integer.parseInt(prop.getProperty("dbmaxconn"));
    	partitionCount = Integer.parseInt(prop.getProperty("partitioncount"),DEFAULT_PARTITIONCOUNT);
    	cacheIp=prop.getProperty("cacheip");
    	cachePort = NumberUtils.toInt(prop.getProperty("cacheport"));
    	preloadPwd=prop.getProperty("preloadpwd");
    	tableScanBasePakage=prop.getProperty("tablescanbasepakage");
    	tablePartition = Integer.parseInt(prop.getProperty("tablepartition"));
    	poolMaxActive = Integer.parseInt(prop.getProperty("poolmaxactive"));
    	poolMaxIdle = Integer.parseInt(prop.getProperty("poolmaxidle"));
    	poolMaxWait = Integer.parseInt(prop.getProperty("poolmaxwait"));
    	updateInterval = Integer.parseInt(prop.getProperty("updateinterval"));
    	updateThreadNum = Integer.parseInt(prop.getProperty("updatethreadnum"));
    	tableStartNum = Integer.parseInt(prop.getProperty("tablestartnum"));
    	cacheExpire = Math.max(1, Integer.parseInt(prop.getProperty("cacheexpire")));
		String info = "dcache.properties info -- databaseIP:" + databaseIP
				+ " databasePort:" + databasePort + " databaseName:"
				+ databaseName + " databaseInitConn:" + databaseInitConn
				+ " databaseMaxConn:" + databaseMaxConn + " partitionCount:"
				+ partitionCount + " cacheAddr:" + cacheIp+":"+cachePort
				+ " tableScanBasePakage:" + tableScanBasePakage
				+ " tablePartition: " + tablePartition
				+ " poolMaxActive: " + poolMaxActive
				+ " poolMaxIdle: " + poolMaxIdle
				+ " poolMaxWait: " + poolMaxWait
				+ " updateThreadNum: " + updateThreadNum
				+ " tableStartNum: " + tableStartNum
				+ " cacheExpire: " + cacheExpire
				+ " updateInterval: " + updateInterval;
		System.out.println("dcache info loaded :" + info);
		
	}
	
	public int getPartitionCount() {
		return partitionCount;
	}

	public String getDatabaseName() {
		return databaseName;
	}

	public String getDatabaseIP() {
		return databaseIP;
	}
	public int getDatabasePort() {
		return databasePort;
	}
	public String getDatabaseUser() {
		return databaseUser;
	}
	public String getDatabasePassword() {
		return databasePassword;
	}
	public int getDatabaseInitConn() {
		return databaseInitConn;
	}
	public int getDatabaseMaxConn() {
		return databaseMaxConn;
	}
	public String getPreloadPwd() {
		return preloadPwd;
	}
	public String getTableScanBasePakage() {
		return tableScanBasePakage;
	}
	public int getTablePartition() {
		return tablePartition;
	}

	public int getUpdateInterval() {
		return updateInterval;
	}

	public int getUpdateThreadNum() {
		return updateThreadNum;
	}

	public int getTableStartNum() {
		return tableStartNum;
	}

	public String getCacheIp() {
		return cacheIp;
	}

	public int getCachePort() {
		return cachePort;
	}

	public int getPoolMaxActive() {
		return poolMaxActive;
	}

	public int getPoolMaxWait() {
		return poolMaxWait;
	}

	public int getPoolMaxIdle() {
		return poolMaxIdle;
	}

	public int getCacheExpire() {
		return cacheExpire;
	}
	
}
