/**
 * 
 */
package com.app.model;

/**
 * @author lisong
 * @version 2013-5-24 下午4:37:36
 */
public class Condition {
	private String orderAttr;
	private String orderDir; 
	private String filterAttr; 
	private String filterValue; 
	private String filterOperator; 
	private String filterType; 
	private long limitOffset; 
	private int limitRange;
	 
	public String getOrderAttr() {
		return orderAttr;
	}
	public void setOrderAttr(String orderAttr) {
		this.orderAttr = orderAttr;
	}
	public String getOrderDir() {
		return orderDir;
	}
	public void setOrderDir(String orderDir) {
		this.orderDir = orderDir;
	}
	public String getFilterAttr() {
		return filterAttr;
	}
	public void setFilterAttr(String filterAttr) {
		this.filterAttr = filterAttr;
	}
	public String getFilterValue() {
		return filterValue;
	}
	public void setFilterValue(String filterValue) {
		this.filterValue = filterValue;
	}
	public String getFilterOperator() {
		return filterOperator;
	}
	public void setFilterOperator(String filterOperator) {
		this.filterOperator = filterOperator;
	}
	public String getFilterType() {
		return filterType;
	}
	public void setFilterType(String filterType) {
		this.filterType = filterType;
	}
	public long getLimitOffset() {
		return limitOffset;
	}
	public void setLimitOffset(long limitOffset) {
		this.limitOffset = limitOffset;
	}
	public int getLimitRange() {
		return limitRange;
	}
	public void setLimitRange(int limitRange) {
		this.limitRange = limitRange;
	}
}
