package com.app.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.app.exception.DCacheException;

public class TableInfo {

	private String tableName;
	private List<ColumnInfo> colInfos = new ArrayList<ColumnInfo>();
	/**
	 * 主键
	 */
	private ColumnInfo key;
	
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	
	public List<ColumnInfo> getColInfos() {
		return colInfos;
	}
	public void addColInfo(ColumnInfo colInfo) {
		if(colInfos.contains(colInfo)) {
			throw new DCacheException("column duplicate name:"+colInfo.getName());
		}
		this.colInfos.add(colInfo);
	}
	
	public ColumnInfo getKey() {
		return key;
	}
	public void setKey(ColumnInfo key) {
		this.key = key;
	}
	public void addAllColInfo(Collection<ColumnInfo> collention) {
		for(ColumnInfo ci : collention) {
			addColInfo(ci);
		}
	}
}
