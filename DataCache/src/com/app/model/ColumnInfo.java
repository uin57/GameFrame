package com.app.model;

import com.app.util.StringUtils;

public class ColumnInfo {

	public static final ColumnInfo LAST_TIME = new ColumnInfo(){{setName("LastTime");setType("timestamp");setIndex(false);setKey(false);setNullable(false);setDef("CURRENT_TIMESTAMP");setExtra("ON UPDATE CURRENT_TIMESTAMP");}};
	
	private String name;
	/**
	 * 数据库类型
	 * */
	private String type;
	
	private boolean index;
	
	private boolean key;
	
	private boolean nullable;
	
	private String def;
	
	private String extra;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isIndex() {
		return index;
	}

	public void setIndex(boolean index) {
		this.index = index;
	}

	public boolean isKey() {
		return key;
	}

	public void setKey(boolean key) {
		this.key = key;
	}

	public boolean isNullable() {
		return nullable;
	}

	public void setNullable(boolean nullable) {
		this.nullable = nullable;
	}

	public String getDef() {
		return def;
	}

	public void setDef(String def) {
		this.def = def;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof ColumnInfo) {
			return ((ColumnInfo) obj).getName().equals(name);
		}
		return super.equals(obj);
	}
	
	public boolean changed(ColumnInfo info) {
		if (this.name.equals(info.name) && this.nullable == info.nullable
				&& StringUtils.equalsIgnoreCase(this.def, info.def) && StringUtils.equalsIgnoreCase(this.extra,info.extra)
				&& this.type.equals(info.type)) {
			return false;
		}
		return true;
	}
}
