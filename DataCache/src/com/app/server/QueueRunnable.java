package com.app.server;

import java.util.concurrent.ScheduledExecutorService;

import org.apache.log4j.Logger;

import com.app.cache.CacheFactory;
import com.app.cache.RedisClient;
import com.app.config.DCacheConfig;
import com.app.db.DBPool;
import com.app.store.AbstractMySqlStore;
import com.app.store.KeyValueStore;

/**
 * 一:更新队列<p>
* 数据放入更新队列分两步<p>
* 1. 采用redis的map数据类型 key为updateQueueTable|数据库名称|数据表名称|数据主键;<p>
* 	value为map形式,map中 放如要更新的数据其中key==>columnName value==>columnValue;若表结构为kv形式则map中key分别为{@link KeyValueStore#COLUMN_KEY},{@link KeyValueStore#COLUMN_VALUE},和各个索引类型的属性的名称<p>
* 2.采用redis的set数据类型 key为updateQueueTable|数据库名称|数据表名称<p>
* 	value为set形式,set的值为第一步中的key(即:updateQueueTable|数据库名称|数据表名称|数据主键)<p>
* <p>
* <p>
* 二:删除队列<p>
* 	删除数据只有一步,采用redis的set数据类型,key为delQueueTable|数据库名称|数据表名称<p>
* 	value为set形式,set的值为待删除的数据主键<p>
* 
* @author lisong_otd
*
*/
public abstract class QueueRunnable implements Runnable {

	protected final Logger logger = Logger.getLogger(getClass());
	public static final int UPDATE_INTERVAL = DCacheConfig.getInstance().getUpdateInterval();
	protected static final DBPool pool = DBPool.getInstance();
	protected final AbstractMySqlStore store;
	protected static final RedisClient client = CacheFactory.getRedisClient();
	protected String tableName;
	protected volatile boolean stop;
	protected volatile boolean done;
	protected final String queueKey;
	protected int errorTimes;
	protected ScheduledExecutorService service;
	
	QueueRunnable(String tableName,AbstractMySqlStore store,String queueKey,ScheduledExecutorService service) {
		this.tableName = tableName;
		this.store = store;
		this.queueKey = queueKey;
		this.service = service;
	}

	public void setStop(boolean stop) {
		this.stop = stop;
	}

}