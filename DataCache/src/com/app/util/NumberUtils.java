package com.app.util;


/**
 * 数字处理工具类
 * 
 * @author sqq
 * 
 */
public class NumberUtils {

	/**
	 * 从字符串转换为 Int 类数字
	 * <p>
	 * 字符串中的数字必须整形数据的10进制表达格式
	 * </p>
	 * 
	 * @param str
	 * @return int
	 */
	public static int toInt(String str) {
		try {
			return Integer.parseInt(str);
		} catch (NumberFormatException nfe) {
			return 0;
		}
	}

	/**
	 * 从字符串转为Int类型数字，必须提供默认值
	 * 
	 * @param str
	 *            要解析的整数
	 * @param defaultValue
	 *            默认值
	 * @return 解析后的数
	 */
	public static int toInt(String str, int defaultValue) {
		int num = defaultValue;
		try {
			num = Integer.parseInt(str);
		} catch (NumberFormatException nfe) {
		}
		return num;
	}

	/**
	 * 从字符串数组转换为 Int 类数字数组
	 * <p>
	 * 字符串中的数字必须整形数据的10进制表达格式
	 * </p>
	 * <p>
	 * 字符串数组任意一项转换失败对应的数字数组中值为0
	 * </p>
	 * 
	 * @param str
	 * @return int[]
	 */
	public static int[] toInt(String[] str) {
		int[] result = new int[str.length];
		for (int i = 0; i < result.length; i++) {
			result[i] = toInt(str[i]);
		}
		return result;
	}

	/**
	 * 
	 * @param obj
	 * @return
	 */
	public static int toInt(Object obj) {
		return toInt(String.valueOf(obj));
	}

	public static float toFloat(String str) {
		try {
			return Float.parseFloat(str);
		} catch (NumberFormatException nfe) {
			return 0f;
		}
	}
	
	/**
	 * 取最小值
	 * 
	 * @param values
	 * @return
	 */
	public static int min(int... values) {
		int min = values[0];
		for (int i = 1; i < values.length; i++) {
			if (values[i] < min) {
				min = values[i];
			}
		}
		return min;
	}

	/**
	 * 取最大值
	 * 
	 * @param values
	 * @return
	 */
	public static int max(int... values) {
		int max = values[0];
		for (int i = 1; i < values.length; i++) {
			if (values[i] > max) {
				max = values[i];
			}
		}
		return max;
	}

	/**
	 * 取最小值
	 * 
	 * @param values
	 * @return
	 */
	public static float min(float... values) {
		float min = values[0];
		for (int i = 1; i < values.length; i++) {
			if (values[i] < min) {
				min = values[i];
			}
		}
		return min;
	}

	/**
	 * 取最大值
	 * 
	 * @param values
	 * @return
	 */
	public static float max(float... values) {
		float max = values[0];
		for (int i = 1; i < values.length; i++) {
			if (values[i] > max) {
				max = values[i];
			}
		}
		return max;
	}

	/**
	 * 取最小值
	 * 
	 * @param values
	 * @return
	 */
	public static double min(double... values) {
		double min = values[0];
		for (int i = 1; i < values.length; i++) {
			if (values[i] < min) {
				min = values[i];
			}
		}
		return min;
	}

	/**
	 * 取最大值
	 * 
	 * @param values
	 * @return
	 */
	public static double max(double... values) {
		double max = values[0];
		for (int i = 1; i < values.length; i++) {
			if (values[i] > max) {
				max = values[i];
			}
		}
		return max;
	}

	/**
	 * 赋值随机值
	 * 
	 * @param rate
	 */
	public static double[] random(double[] rate) {
		for (int i = 0; i < rate.length; i++) {
			rate[i] = Math.random();
		}
		return rate;
	}

	/**
	 * 摊平随机比, 实所有数合=1
	 * 
	 * @param rate
	 */
	public static double[] averageRate(double[] rate) {
		double d = 0;
		for (int i = 0; i < rate.length; i++) {
			d += rate[i];
		}
		for (int i = 0; i < rate.length; i++) {
			rate[i] /= d;
		}
		return rate;
	}

	/**
	 * double化为百分数
	 * 
	 * @param value
	 * @param decimalPlaces
	 * @author liujinye
	 * @return
	 */
	// public static double doublePercentage(double value,int decimalPlaces){
	// value = doubleRounding(value, decimalPlaces + 2);
	// value = value * 100;
	// return value;
	// }

	public static long toLong(String str) {
		long id = 0;
		try {
			id = Long.parseLong(str);
		} catch (NumberFormatException e) {
			return 0;
		}
		return id;
	}

	/**
	 * 标识随机种子用于解决同一个毫秒内产生数不相同
	 */
	static int seed = 0xFFFE;

	/**
	 * 产生唯一标示
	 * <p>
	 * 由时间戳的后47位左移16位与16位自增数合并而成总共63位<br />
	 * 因为第一位为符号位,因此只使用了63位<br />
	 * 保证1ms内产生65535个不重复的数,如果要保证产生更多个数,可增加自增部分的位数
	 * </p>
	 * 
	 * @return
	 */
	public static long nextIdentity() {
		return ((System.currentTimeMillis() & 0x7FFFFFFFFFFFl) << 16)
				| (seed++ & 0xFFFF);
	}

	public static void main(String[] args) {
		System.out.println(nextIdentity());
	}
}
