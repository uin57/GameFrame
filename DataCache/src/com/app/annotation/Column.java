package com.app.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Column {

	static final int LENGTH = 50;
	
	static final boolean NULLABLE = true;
	
	static final String DEFAULT = "";

	String name() default "";
	
	int length() default LENGTH;
	
	boolean nullable() default NULLABLE;
	
	String Default() default DEFAULT;
}
