/**
 * 
 */
package cn.x6game.socket;

import java.util.concurrent.Executor;

import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.handler.execution.ExecutionHandler;

import cn.x6game.socket.handler.SocketCSUpstreamHandler;

import com.app.framework.socket.codec.PacketDecoder;
import com.app.framework.socket.codec.PacketEncoder;

/**
 * @author lisong
 * @version 2013-2-26 下午5:59:52
 */
public class SocketPipelineFactory implements ChannelPipelineFactory {
	 
	private final ExecutionHandler execution;
	
	public SocketPipelineFactory(Executor executor) {
		
		this.execution = new ExecutionHandler(executor);
		
	}

	private final SocketCSUpstreamHandler handler = new SocketCSUpstreamHandler();
	
	@Override
	public ChannelPipeline getPipeline() throws Exception {
		ChannelPipeline pipeline=Channels.pipeline();
		pipeline.addLast("decoder", new PacketDecoder());
		pipeline.addLast("encode", new PacketEncoder());
		pipeline.addLast("executor", execution);
		pipeline.addLast("handler", handler);
		return pipeline;
	}
	
}
