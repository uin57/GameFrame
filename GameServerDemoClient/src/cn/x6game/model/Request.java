/**
 * 
 */
package cn.x6game.model;



/**
 * @author lisong
 * @version 2013-2-27 下午4:48:10
 */
public interface Request {
	/**
	 * action 服务的名字
	 */
	String ACTION_SERVICE_NAME = "action";
	
	public int getType();
	
	public byte[] getContent();
	
	public String getServiceName();
	
	public void setSessionId(String sessionId);
	
	public String getSessionId();
	
	public void setRequestId(int requestId);
	
	public int getRequestId();
	
}
