/**
 * 
 */
package cn.x6game.model;


/**
 * @author lisong
 * @version 2013-3-29 上午11:44:37
 */
public abstract class AbstractRequest implements Request {

	private byte[] content;

	private String serviceName;
	
	private int type;
	
	private String sessionId;
	
	private int requestId;
	
	public AbstractRequest(byte[] content,String serviceName,int type) {
		this.content = content;
		this.serviceName = serviceName;
		this.type = type;
	}
	
	@Override
	public String getServiceName() {
		return serviceName;
	}

	public byte[] getContent() {
		return content;
	}

	@Override
	public int getType() {
		
		return type;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public int getRequestId() {
		return requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}
}
