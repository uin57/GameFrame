/**
 * 
 */
package cn.x6game.model;



/**
 * @author lisong
 * @version 2013-2-28 上午11:21:38
 */
public class ActionRequest extends AbstractRequest {
	
	public static final int TYPE = 2;
	
	public ActionRequest(byte[] content,String actionName,String userUid) {
		
		super(content, ACTION_SERVICE_NAME, TYPE);
		
		this.actionName = actionName;
		
		this.userUid = userUid;
	}

	private String actionName;
	
	private String userUid;
	
	public String getActionName() {
		return actionName;
	}

	public String getUserUid() {
		return userUid;
	}

}
