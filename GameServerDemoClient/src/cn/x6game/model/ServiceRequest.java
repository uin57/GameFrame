/**
 * 
 */
package cn.x6game.model;


/**
 * @author lisong
 * @version 2013-2-28 上午11:21:38
 */
public class ServiceRequest extends AbstractRequest {

	public static final int TYPE = 1;
	
	public ServiceRequest(byte[] content,String serviceName,String methodName) {
		super(content, serviceName, TYPE);
		this.methodName = methodName;
	}
	private String methodName;

	public String getMethodName() {
		return methodName;
	}
}
