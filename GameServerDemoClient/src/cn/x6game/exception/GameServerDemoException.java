package cn.x6game.exception;

public class GameServerDemoException extends RuntimeException{
	
	private static final long serialVersionUID = 1L;
	
	public GameServerDemoException(String msg) {
		super(msg);
	}
	
	public GameServerDemoException(String msg,Exception e) {
		super(msg, e);
	}
	
	public GameServerDemoException(Exception e) {
		super(e);
	}
}
