/**
 * 
 */
package cn.x6game.request;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import cn.x6game.model.ActionRequest;
import cn.x6game.model.Request;
import cn.x6game.model.ServiceRequest;
import cn.x6game.util.Config;
import cn.x6game.util.JSONUtil;

/**
 * @author lisong
 * @version 2013-2-27 下午3:14:12
 */
public abstract class AbstractRequest {

	private static String uri = Config.getServerUrl();
	
	/**
	 * header 参数
	 * 
	 * 请求字符串的名称  cmd
	 * 参数json串长度的名称 params-length
	 * 请求的参数的名称 params
	 * 请求序列号 reqId
	 * 
	 * 返回:
	 * 请求字符串的名称  cmd
	 * 参数json串长度的名称 result-length
	 * 返回的数据 result
	 * 请求序列号 reqId
	 * @param request
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public String request(DefaultHttpClient client,Request request) throws Exception {
		HttpPost httpPost=new HttpPost(uri);
		if(request.getType() == 2) {
			httpPost.addHeader("action", ((ActionRequest) request).getActionName());
			httpPost.addHeader("user", ((ActionRequest) request).getUserUid());
			httpPost.addHeader("reqid", ((ActionRequest) request).getRequestId()+"");
		}
		else {
			httpPost.addHeader("service",request.getServiceName());
			httpPost.addHeader("method", ((ServiceRequest)request).getMethodName());
		}
		
		httpPost.addHeader("cmd", request.getType()+"");
		if(request.getContent() != null) {
			HttpEntity entity = new ByteArrayEntity(request.getContent());
			httpPost.setEntity(entity);
		}
		HttpResponse response = client.execute(httpPost);
		InputStream in = new GZIPInputStream(response.getEntity().getContent());
		BufferedReader reader = new BufferedReader(new InputStreamReader(in,"UTF-8"));
		String result = reader.readLine();
		reader.close();
		Map<String,Object> resultMap = JSONUtil.toMap(result);
		try {
			if((Integer)resultMap.get("code") != 200) {
				System.err.println("msg:"+resultMap.get("message")+" code:"+resultMap.get("code"));
			}else {
				if(request.getType() == 2) {
					Header h = response.getFirstHeader("reqid");
					if(h != null) {
						return h.getValue();
					}
					else {
						return null;
					}
				}
				Object ret = null;
				ret = resultMap.get("data"); 
				if( ret != null) {
					return ret.toString(); 
				}
			}
			
		}finally {
			httpPost.releaseConnection();
		}
		return null;
	}
	
	public HttpResponse request2(DefaultHttpClient client,Request request) throws Exception {
		HttpPost httpPost = null;
		HttpResponse response = null;
		try {
			httpPost=new HttpPost(uri);
			String cmd = "1";
			if(request instanceof ActionRequest) {
				cmd = "2";
			}
			String json = JSONUtil.toJSON(request);
			httpPost.addHeader("cmd", cmd);
			httpPost.addHeader("params-length", ""+ json.length());
			
			HttpEntity entity = new ByteArrayEntity(json.getBytes("UTF-8"));
			httpPost.setEntity(entity);
			response = client.execute(httpPost);
		
		}catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
		return response;
	}
}
