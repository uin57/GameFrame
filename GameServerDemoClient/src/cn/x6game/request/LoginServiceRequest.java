package cn.x6game.request;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.impl.client.DefaultHttpClient;

import cn.x6game.model.ServiceRequest;
import cn.x6game.util.JSONUtil;


public class LoginServiceRequest extends AbstractRequest{
	
	/**
	 * 登录
	 * @throws Exception
	 */
	public String login(DefaultHttpClient client,String id) throws Exception {
		Map<String,String> map = new HashMap<String, String>();
		map.put("userUid", id);
		map.put("pwd", "123456");
		ServiceRequest request = new ServiceRequest(JSONUtil.toJSON(map).getBytes(),"userProfile","login");
		return super.request(client,request);
	}
	
	public static void main(String[] args) {
		LoginServiceRequest request = new LoginServiceRequest();
		DefaultHttpClient client = new DefaultHttpClient();
		client.getParams().setParameter(ClientPNames.COOKIE_POLICY,  CookiePolicy.BROWSER_COMPATIBILITY); //s设置cookie的兼容性
		try {
//			request.login(client);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
