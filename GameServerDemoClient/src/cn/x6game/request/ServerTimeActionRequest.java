package cn.x6game.request;

import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.impl.client.DefaultHttpClient;

import cn.x6game.model.ActionRequest;

public class ServerTimeActionRequest extends AbstractRequest{
	
	//一次action 请求时间
	
	public int getTime(DefaultHttpClient client,String id,String sessionId,int requestId) throws Exception {
		
		ActionRequest request = new ActionRequest(null,"actionTime",id);
		request.setRequestId(requestId);
		request.setSessionId(sessionId);
		String ret = super.request(client,request);
		if(ret == null) {
			return 0;
		}
		return Integer.parseInt(ret);
	}
	
	public static void main(String[] args) {
		ServerTimeActionRequest request = new ServerTimeActionRequest();
		DefaultHttpClient client = new DefaultHttpClient();
		client.getParams().setParameter(ClientPNames.COOKIE_POLICY,  CookiePolicy.BROWSER_COMPATIBILITY); //s设置cookie的兼容性
		try {
//			request.getTime(client);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
