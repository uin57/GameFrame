package cn.x6game.request;

import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.impl.client.DefaultHttpClient;

import cn.x6game.model.ServiceRequest;

public class ServerTimeServiceRequest extends AbstractRequest{
	
	//一次action 请求时间
	
	public void getTime(DefaultHttpClient client) throws Exception {
		ServiceRequest request = new ServiceRequest(null,"userProfile","serviceTime");
		super.request(client,request);
	}
	
	public static void main(String[] args) {
		ServerTimeServiceRequest request = new ServerTimeServiceRequest();
		DefaultHttpClient client = new DefaultHttpClient();
		client.getParams().setParameter(ClientPNames.COOKIE_POLICY,  CookiePolicy.BROWSER_COMPATIBILITY); //s设置cookie的兼容性
		try {
//			request.getTime(client);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
