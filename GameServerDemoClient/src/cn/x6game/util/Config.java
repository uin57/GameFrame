package cn.x6game.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import cn.x6game.exception.GameServerDemoException;


public class Config {
	private static Map<String,String> idSessionMap = new ConcurrentHashMap<String, String>();
	private static String serverUrl;
	static {
		idSessionMap = new ConcurrentHashMap<String, String>();
		readProp();
		load();
	}
	private Config() {
	}
	/**
	 * 
	 */
	private static void load() {
		try {
			Reader reader = null;
			reader = new InputStreamReader(new FileInputStream(System.getProperty("user.dir")+"/config.properties"),"UTF-8");
	    	Properties prop=new Properties();
	    	prop.load(reader);
	    	reader.close();
	    	serverUrl = prop.getProperty("serverurl");
		}catch (Exception e) {
			e.printStackTrace();
			throw new GameServerDemoException(e);
		}
	}
	private static void readProp() {
		try {
			Reader reader = null;
			reader = new InputStreamReader(new FileInputStream(System.getProperty("user.dir")+"/idSession.properties"),"UTF-8");
	    	Properties prop=new Properties();
	    	prop.load(reader);
	    	reader.close();
	    	for(Entry<Object, Object> entity : prop.entrySet()) {
	    		idSessionMap.put(entity.getKey().toString(), entity.getValue().toString());
	    	}
		}catch (Exception e) {
			e.printStackTrace();
			throw new GameServerDemoException(e);
		}
	}
	public static Map<String, String> getIdSessionMap() {
		return idSessionMap;
	}
	public static void writeProp() throws IOException {
		Properties prop = new Properties();
		for(String key:idSessionMap.keySet()) {
			String value = idSessionMap.get(key);
			if(value == null) {
				value = "";
			}
			prop.setProperty(key, value);
		}
		String path =  System.getProperty("user.dir")+"/idSession.properties";
		OutputStream out = new FileOutputStream(path,false);
		prop.store(out, null);
		out.close();
	
	}
	
	public static String getServerUrl() {
		return serverUrl;
	}
}	
