package cn.x6game.util;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author lisong
 * @version 2013-2-28 下午4:05:45
 */
public class StringUtils {

	/**
	 * 截取字符左边
	 * 
	 * @param str
	 *            被截取的字符串
	 * @param size
	 *            截取字符串的最大长度
	 * @return 返回被截取的字符，最大不超过指定长度
	 */
	public static String left(String str, int size) {
		if (str == null || size <= 0) {
			return "";
		}
		if (size > str.length()) {
			size = str.length();
		}
		return str.substring(0, size);
	}

	/**
	 * 截取字符右边
	 * 
	 * @param str
	 *            被截取的字符串
	 * @param size
	 *            截取字符串的最大长度
	 * @return 返回被截取的字符，最大不超过指定长度
	 */
	public static String right(String str, int size) {
		if (str == null || size <= 0) {
			return "";
		}
		int start = str.length() - size;
		if (start < 0) {
			start = 0;
			size = str.length();
		}
		return str.substring(start, str.length());
	}

	/**
	 * 判断字符串<br/>
	 * 
	 * @param str
	 * @return 如果字符串为null或者为空串或者全部为空格组成则返回true反之则返回false;<br/>
	 */
	public static boolean isNullOrEmpty(String str) {
		return str == null || str.isEmpty() || str.trim().isEmpty();
	}
	
	public static boolean equalsIgnoreCase(String str1,String str2) {
		if(str1 == null && str2 == null) {
			return true;
		}
		else if(str1 != null && str2 != null) {
			return str1.equalsIgnoreCase(str2);
		}
		else if(isNullOrEmpty(str1) && isNullOrEmpty(str2)) {
			return true;
		}
		return false;
	}
	
	/**
	 * 如果数组中有任意元素为空 则为true
	 * @param str
	 * @return
	 */
	public static boolean isNullOrEmpty(String... str) {
		for(String tmp : str) {
			if(isNullOrEmpty(tmp)) {
				return true;
			}
		}
		return false;
	}
	/**
	 * 检查参数是否存在
	 * 
	 * @param parameters
	 * @param para
	 * @return
	 */
	public static boolean checkParameters(Map<String, Object> parameters,
			String... para) {
		for (String str : para) {
			if (!parameters.containsKey(str)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 如果str为null 则转换成空串,否则原样输出
	 * @param str
	 * @return
	 */
	public static String null2Empty(String str) {
		if (isNullOrEmpty(str)) {
			return "";
		}
		return str;
	}
	
	public static boolean parseBoolean(String str) {
		if (isNullOrEmpty(str)) {
			return false;
		}
		if (Boolean.parseBoolean(str) || str.equals("1")) {
			return true;
		}
		return false;
	}

	/**
	 * 验证邮箱
	 * 
	 * @param 待验证的字符串
	 * @return 如果是符合邮箱格式的字符串,返回<b>true</b>,否则为<b>false</b>
	 */
	public static boolean email(String str) {
		if (StringUtils.isNullOrEmpty(str)) {
			return false;
		}
		String regex = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(str);
		return matcher.matches();
	}
}
