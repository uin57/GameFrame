package test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.impl.client.DefaultHttpClient;

import cn.x6game.request.ServerTimeActionRequest;
import cn.x6game.t12.multithead.Task;
import cn.x6game.t12.multithead.TaskRunner;
import cn.x6game.util.Config;

public class ActionTimeTest implements Task{
	
	private int count;
	private String id;
	private String sessionId;
	private int requestId ;
	DefaultHttpClient client = null;
	
	public ActionTimeTest(int count,String id,String sessionId) {
		this.count = count;
		this.id = id;
		this.sessionId = sessionId;
		requestId = 0;
	}

	@Override
	public void done() {
		client.getConnectionManager().shutdown();
	}

	@Override
	public int getCount() {
		return count;
	}

	@Override
	public void prepare() {
		client = new DefaultHttpClient();
		client.getParams().setParameter(ClientPNames.COOKIE_POLICY,  CookiePolicy.BROWSER_COMPATIBILITY); //s设置cookie的兼容性
	}

	@Override
	public void runOnce() {
		ServerTimeActionRequest request = new ServerTimeActionRequest();
		try {
			requestId += 1;
			requestId = request.getTime(client, id, sessionId,requestId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		int threads = Integer.parseInt(args[0]);
		int dup = Integer.parseInt(args[1]);
		TaskRunner tr = new TaskRunner();
		List<String> keys = new ArrayList<String>(Config.getIdSessionMap().keySet());
		for (int i = 0; i < threads; i++) {
			String uid = keys.get(i);
			String sessionId = Config.getIdSessionMap().get(uid);
			ActionTimeTest t = new ActionTimeTest(dup,uid,sessionId);
			tr.addTask(t);
		}
		tr.setRunSpeed(threads);
		tr.startAll();
		tr.waitToEnd();
	}

}
