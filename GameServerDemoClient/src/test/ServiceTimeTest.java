package test;

import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.impl.client.DefaultHttpClient;

import cn.x6game.request.ServerTimeServiceRequest;
import cn.x6game.t12.multithead.Task;
import cn.x6game.t12.multithead.TaskRunner;

public class ServiceTimeTest implements Task{
	
	private int count;
	DefaultHttpClient client = null;
	
	public ServiceTimeTest(int count) {
		this.count = count;
	}

	@Override
	public void done() {
		client.getConnectionManager().shutdown();
	}

	@Override
	public int getCount() {
		return count;
	}

	@Override
	public void prepare() {
		client = new DefaultHttpClient();
		client.getParams().setParameter(ClientPNames.COOKIE_POLICY,  CookiePolicy.BROWSER_COMPATIBILITY); //s设置cookie的兼容性
	}

	@Override
	public void runOnce() {
		ServerTimeServiceRequest request = new ServerTimeServiceRequest();
		try {
			request.getTime(client);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		int threads = Integer.parseInt(args[0]);
		int dup = Integer.parseInt(args[1]);
		TaskRunner tr = new TaskRunner();
		for (int i = 0; i < threads; i++) {
			ServiceTimeTest t = new ServiceTimeTest(dup);
			tr.addTask(t);
		}
		tr.startAll();
		tr.waitToEnd();
	}

}
