package test;

import java.io.IOException;
import java.util.UUID;

import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.impl.client.DefaultHttpClient;

import cn.x6game.request.RegisterServiceRequest;
import cn.x6game.t12.multithead.Task;
import cn.x6game.t12.multithead.TaskRunner;
import cn.x6game.util.Config;
import cn.x6game.util.StringUtils;

public class RegisterTest implements Task{

	private int count;
	DefaultHttpClient client = null;
	private String userUid;
	private String name;
	public RegisterTest(int count) {
		this.count = count;
	}
	
	public RegisterTest(String name,int count) {
		this.count = count;
		this.name = name;
	}
	
	@Override
	public void done() {
		client.getConnectionManager().shutdown();
		if(!StringUtils.isNullOrEmpty(userUid)) {
			Config.getIdSessionMap().put(userUid,"");
		}
	}

	@Override
	public int getCount() {
		return this.count;
	}

	@Override
	public void prepare() {
		client = new DefaultHttpClient();
		client.getParams().setParameter(ClientPNames.COOKIE_POLICY,  CookiePolicy.BROWSER_COMPATIBILITY); //s设置cookie的兼容性
		
	}

	@Override
	public void runOnce() {
		RegisterServiceRequest request = new RegisterServiceRequest();
		try {
			userUid = request.register(client,name);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		Config.getIdSessionMap().clear();
		int threads = Integer.parseInt(args[0]);
		int dup = Integer.parseInt(args[1]);
		TaskRunner tr = new TaskRunner();
		for (int i = 0; i < threads; i++) {
			RegisterTest t = new RegisterTest(UUID.randomUUID().toString(),dup);
			tr.addTask(t);
		}
		tr.startAll();
		tr.waitToEnd();
		try {
			Config.writeProp();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
