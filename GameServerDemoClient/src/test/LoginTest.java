package test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.impl.client.DefaultHttpClient;

import cn.x6game.request.LoginServiceRequest;
import cn.x6game.t12.multithead.Task;
import cn.x6game.t12.multithead.TaskRunner;
import cn.x6game.util.Config;
import cn.x6game.util.StringUtils;

public class LoginTest implements Task{
	
	private int count;
	private String id;
	DefaultHttpClient client = null;
	private String sessionId;
	
	public LoginTest(int count,String id) {
		this.count = count;
		this.id = id;
	}

	@Override
	public void done() {
		client.getConnectionManager().shutdown();
		if(!StringUtils.isNullOrEmpty(sessionId)) {
			Config.getIdSessionMap().put(id, sessionId);
		}
	}

	@Override
	public int getCount() {
		return this.count;
	}

	@Override
	public void prepare() {
		client = new DefaultHttpClient();
		client.getParams().setParameter(ClientPNames.COOKIE_POLICY,  CookiePolicy.BROWSER_COMPATIBILITY); //s设置cookie的兼容性
	}

	@Override
	public void runOnce() {

		LoginServiceRequest request = new LoginServiceRequest();
		try {
			sessionId = request.login(client, id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {
		int threads = Integer.parseInt(args[0]);
		int dup = Integer.parseInt(args[1]);
		TaskRunner tr = new TaskRunner();
		try {
			List<String> keys = new ArrayList<String>(Config.getIdSessionMap().keySet());
			for (int i = 0; i < threads; i++) {
				LoginTest t = new LoginTest(dup,keys.get(i));
				tr.addTask(t);
			}
			tr.startAll();
			tr.waitToEnd();
			Config.writeProp();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
