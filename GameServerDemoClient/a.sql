drop database idx;
create database idx default CHARACTER SET "utf8" ;

use idx;

create table idx(
	k varchar(32),
	p varchar(32),
	vs int ,
	v varchar(1024),
	primary key (k,p)
)  engine='myisam';