package com.app.service.action;

import java.io.Serializable;

import com.app.framework.http.HttpContext;
import com.app.framework.result.RequestResult;


public abstract class HttpAction implements GameAction<HttpContext> {
	
	public abstract RequestResult<? extends Serializable> execute(HttpContext context) throws Exception;

}
