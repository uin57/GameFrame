package com.app.service.action;

import org.apache.log4j.Logger;

import com.app.framework.socket.SocketContext;

public abstract class SocketAction implements GameAction<SocketContext> {
	
	protected Logger logger = Logger.getLogger(getClass());
	
	public abstract void execute(SocketContext context) throws Exception;

}
