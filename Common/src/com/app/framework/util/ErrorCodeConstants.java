/**
 * 
 */
package com.app.framework.util;

/**
 * @author lisong
 * @version 2013-3-13 上午11:38:41
 */
public class ErrorCodeConstants {

	/**===================以下为客户端错误=====================*/
	/**
	 * 客户端service请求参数信息不完整
	 */
	public static final int CLIENT_ERROR_SERVICE_REQUEST_INCOMPLETE = 401;
	
	/**
	 * 客户端action请求参数信息不完整
	 */
	public static final int CLIENT_ERROR_ACTION_REQUEST_INCOMPLETE = 402;
	/**
	 * 客户端请求的服务不存在
	 */
	public static final int CLIENT_ERROR_SERVICE_NAME_NOT_EXIST = 403;
	/**
	 * 客户端请求的服务方法不存在
	 */
	public static final int CLIENT_ERROR_SERVICE_METHOD_NAME_NOT_EXIST = 404;
	/**
	 * 客户端请求已经被执行过
	 */
	public static final int CLIENT_ERROR_REQUEST_EXECUTED = 405;
	
	/**
	 * 客户端请求的action不存在
	 */
	public static final int CLIENT_ERROR_ACTION_NAME_NOT_EXIST = 407;
	/**
	 * 客户端请求过快
	 */
	public static final int CLIENT_ERROR_ACTION_REQUEST_TOO_FAST = 408;
	/**
	 * session失效
	 */
	public static final int CLIENT_ERROR_SESSION_EXPIRED = 409;
	/**
	 * session没有创建
	 */
	public static final int CLIENT_ERROR_SESSION_NOT_CREATE = 410;
	
	/**
	 * 已经异地登录
	 */
	public static final int CLIENT_ERROR_OTHER_LOGIN = 411;
	
	/**
	 * 客户端版本太低
	 */
	public static final int CLIENT_VER_TOO_LOW = 412;
	
	/**
	 * 请求cmd为空或null
	 */
	public static final int CLIENT_ERROR_CMD_NOT_CONTAIN = 413;
	
	/**
	 * userid为空
	 */
	public static final int CLIENT_ERROR_USERID_NULL = 414;
	
	/**=========================以下为服务器错误========================== */
	
	/**
	 * session创建失败
	 */
	public static final int SERVER_ERROR_SESSION_CREATE_FAIL = 501;
	/**
	 * 资源对比xml文件不存在或者文件中没有数据
	 */
	public static final int SERVER_ERROR_RESCOMP_NOT_EXIST_OR_NO_DATA = 502;

}
