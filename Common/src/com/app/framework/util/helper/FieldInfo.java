package com.app.framework.util.helper;

import java.lang.reflect.Field;

public class FieldInfo {
	private Field field;
	private boolean  byReference;
	private boolean byValue;
	private boolean jsonableCollection;//可json的集合 包含 list map
	private boolean jsonableArray;
	
	public boolean isJsonableArray() {
		return jsonableArray;
	}
	public void setJsonableArray(boolean jsonableArray) {
		this.jsonableArray = jsonableArray;
	}
	public Field getField() {
		return field;
	}
	public void setField(Field field) {
		this.field = field;
	}
	public boolean isByReference() {
		return byReference;
	}
	public void setByReference(boolean byReference) {
		this.byReference = byReference;
	}
	public boolean isByValue() {
		return byValue;
	}
	public void setByValue(boolean byValue) {
		this.byValue = byValue;
	}
	public boolean isJsonableCollection() {
		return jsonableCollection;
	}
	public void setJsonableCollection(boolean jsonableCollection) {
		this.jsonableCollection = jsonableCollection;
	}
}
