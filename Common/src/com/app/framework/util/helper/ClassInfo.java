package com.app.framework.util.helper;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ClassInfo {
	
	private Class<?> clz;
	/**
	 * 需要给客户端传输的数据
	 */
	private Map<String,FieldInfo> transferFields = new LinkedHashMap<String, FieldInfo>();
	private Map<String,FieldInfo> persistFields = new LinkedHashMap<String, FieldInfo>();
	private FieldInfo idField;
	private Map<String,FieldInfo> indexFields = new LinkedHashMap<String, FieldInfo>();
	
	private List<Method> cascadeRemoveMethods = new ArrayList<Method>();
	
	public Class<?> getClz() {
		return clz;
	}
	public void setClz(Class<?> clz) {
		this.clz = clz;
	}
	public List<Method> getCascadeRemoveMethods() {
		return cascadeRemoveMethods;
	}
	public void setCascadeRemoveMethods(List<Method> cascadeRemoveMethods) {
		this.cascadeRemoveMethods = cascadeRemoveMethods;
	}
	public Map<String, FieldInfo> getTransferFields() {
		return transferFields;
	}
	public Map<String, FieldInfo> getPersistFields() {
		return persistFields;
	}
	public FieldInfo getIdField() {
		return idField;
	}
	public void setIdField(FieldInfo idField) {
		this.idField = idField;
	}
	public Map<String, FieldInfo> getIndexFields() {
		return indexFields;
	}
}
