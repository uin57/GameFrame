/**
 * 
 */
package com.app.framework.util;


import org.apache.log4j.Logger;

import com.app.framework.context.Request;
import com.app.framework.exception.CommunicationException;

/**
 * @author lisong
 * @version 2013-2-27 下午4:46:05
 */
public class ParamUtil {

	public static final String DEFAULT_CHARSET = "UTF-8";
	public static final String PARAMS_SPLIT = "&";
	public static final String KV_SPLIT = "=";
	private static final Logger logger = Logger.getLogger(ParamUtil.class);
	/**
	 * data的格式如下 xxxkey1=xxxvalue1&xxxkey2=xxxvalue2
	 * @param data
	 * @return
	 * @throws Exception 
	 */
	public static Request parseParam(Class<? extends Request> clz,String data) {
		try {
			if(StringUtils.isNullOrEmpty(data)) {
				return clz.newInstance();
			}
			Request param = JSONUtil.toObject(data, clz);
			return param;
		}catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			throw new CommunicationException(e);
		}
	}
}
