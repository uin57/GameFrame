/**
 * 
 */
package com.app.framework.util;

import org.apache.commons.codec.binary.Base64;

/**
 * @author lisong
 * @version 2013-3-14 下午4:58:48
 */
public class Base64Util {

	public static final String CHARSET = "UTF-8";
	
	public static String decode(String src) throws Exception {
		return new String(Base64.decodeBase64(src),CHARSET);
	}
	
	public static String encode(String src) throws Exception {
		return new String(Base64.encodeBase64String(src.getBytes(CHARSET)));
	}
	
}
