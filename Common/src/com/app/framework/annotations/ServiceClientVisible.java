/**
 * 
 */
package com.app.framework.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * service方法上加上该注解客户端才可访问
 * @author lisong
 * @version 2013-3-13 下午2:50:22
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface ServiceClientVisible {

}
