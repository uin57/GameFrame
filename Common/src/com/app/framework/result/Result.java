/**
 * 
 */
package com.app.framework.result;

import java.io.Serializable;

/**
 * @author lisong
 * @version 2013-3-1 下午3:50:49
 */
public interface Result extends Serializable {

	Serializable asResult();
}
