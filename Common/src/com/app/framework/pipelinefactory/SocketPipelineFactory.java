/**
 * 
 */
package com.app.framework.pipelinefactory;

import java.util.concurrent.Executor;

import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.handler.execution.ExecutionHandler;

import com.app.framework.handler.GameUpstreamHandler;
import com.app.framework.socket.codec.PacketDecoder;
import com.app.framework.socket.codec.PacketEncoder;

/**
 * @author lisong
 * @version 2013-2-26 下午5:59:52
 */
public class SocketPipelineFactory implements GamePipelineFactory {
	 
	private final ExecutionHandler execution;
	
	private final GameUpstreamHandler handler;

	public SocketPipelineFactory(Executor executor,GameUpstreamHandler handler) {
		
		this.execution = new ExecutionHandler(executor);
		this.handler = handler;
	}
	
	@Override
	public ChannelPipeline getPipeline() throws Exception {
		ChannelPipeline pipeline=Channels.pipeline();
		pipeline.addLast("decoder", new PacketDecoder());
		pipeline.addLast("encode", new PacketEncoder());
		pipeline.addLast("executor", execution);
		pipeline.addLast("handler", handler);
		return pipeline;
	}
	
}
