/**
 * 
 */
package com.app.framework.pipelinefactory;

import org.jboss.netty.channel.ChannelPipelineFactory;

/**
 * @author lisong
 * @version 2013-2-26 下午5:59:25
 */
public interface GamePipelineFactory extends ChannelPipelineFactory {

}
