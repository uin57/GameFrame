/**
 * 
 */
package com.app.framework.pipelinefactory;

import java.util.concurrent.Executors;

import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.handler.codec.http.HttpRequestDecoder;
import org.jboss.netty.handler.codec.http.HttpResponseEncoder;
import org.jboss.netty.handler.execution.ExecutionHandler;

import com.app.framework.handler.GameUpstreamHandler;

/**
 * @author lisong
 * @version 2013-2-26 下午5:59:52
 */
public class HttpPipelineFactory implements GamePipelineFactory {
	 
	//private final ExecutionHandler execution = new ExecutionHandler(new MemoryAwareThreadPoolExecutor(2048, 1048576, 1048576));
	private final ExecutionHandler execution = new ExecutionHandler(Executors.newCachedThreadPool());

	private final GameUpstreamHandler handler;
	
	public HttpPipelineFactory(GameUpstreamHandler handler) {
		this.handler = handler;
	}
	
	@Override
	public ChannelPipeline getPipeline() throws Exception {
		ChannelPipeline pipeline=Channels.pipeline();
		pipeline.addLast("decoder", new HttpRequestDecoder(4096,8192,65536));//第三个参数设置chunk分块大小,解决post数据过大后会分块的情况
		pipeline.addLast("encode", new HttpResponseEncoder());
		pipeline.addLast("executor", execution);
		pipeline.addLast("handler", handler);
		return pipeline;
	}

}
