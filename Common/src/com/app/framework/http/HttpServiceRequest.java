/**
 * 
 */
package com.app.framework.http;

import com.app.framework.context.AbstractRequest;


/**
 * @author lisong
 * @version 2013-2-28 上午11:21:38
 */
public class HttpServiceRequest extends AbstractRequest {

	public static final int TYPE = 1;
	
	public HttpServiceRequest(byte[] content,short serviceId,String methodName,String remoteAddress) {
		
		super(content,remoteAddress,TYPE);
		
		this.methodName = methodName;
		
		this.serviceId = serviceId;
	}
	private String methodName;

	private short serviceId;

	public short getServiceId() {
		return serviceId;
	}

	public String getMethodName() {
		return methodName;
	}
	
}
