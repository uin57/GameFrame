/**
 * 
 */
package com.app.framework.context;



/**
 * @author lisong
 * @version 2013-2-27 下午4:48:10
 */
public interface Request {
	/**
	 * action 服务的名字
	 */
	short ACTION_SERVICE_ID = 1;
	
	short SESSION_SERVICE_ID = 2;
	
	short RESOURCE_SERVICE_ID = 3;
	
	public int getType();
	
	public byte[] getContent();
	
	public String getRemoteAddress();
	
}
