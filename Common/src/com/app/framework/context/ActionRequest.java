/**
 * 
 */
package com.app.framework.context;



/**
 * @author lisong
 * @version 2013-2-28 上午11:21:38
 */
public class ActionRequest extends AbstractRequest {
	
	public static final int TYPE = 2;
	
	public ActionRequest(byte[] content,short actionId,long userId,String remoteAddress) {
		
		super(content,remoteAddress,TYPE);
		
		this.actionId = actionId;
		
		this.userId = userId;
	}
	
	public ActionRequest(byte[] content,short actionId,String remoteAddress) {
		this(content, actionId, 0, remoteAddress);
	}

	private short actionId;
	
	private long userId;
	
	private int requestId;
	
	private String sessionId;
	
	public short getActionId() {
		return actionId;
	}

	public long getUserId() {
		return userId;
	}

	public int getRequestId() {
		return requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
}
