/**
 * 
 */
package com.app.framework.context;


/**
 * @author lisong
 * @version 2013-3-29 上午11:44:37
 */
public abstract class AbstractRequest implements Request {

	private byte[] content;

	private String remoteAddress;
	
	private int type;
	
	public AbstractRequest(byte[] content,String remoteAddress,int type) {
		this.content = content;
		this.remoteAddress = remoteAddress;
		this.type = type;
	}
	
	public byte[] getContent() {
		return content;
	}

	public String getRemoteAddress() {
		return remoteAddress;
	}
	@Override
	public int getType() {
		
		return type;
	}
}
