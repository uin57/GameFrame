/**
 * 
 */
package com.app.framework.context;

import com.app.framework.result.RequestResult;


/**
 * @author lisong
 * @version 2013-3-4 下午3:26:16
 */
public interface Response<T> {
	
	String RESPONSE_CHARSET = "UTF-8";

	public Request getRequest();
	
	public void setRequest(Request request);
	/**
	 * 写入到管道
	 */
	public void output() throws Exception;
	/**
	 * 关闭所有
	 */
	public void close();
	
	public void setRequestResult(RequestResult<T> result);
	
	public RequestResult<T> getRequestResult();
	/**
	 * 协议传输异常
	 * @param code
	 * @param message
	 */
	public void protocolException(int code,T message);

}
