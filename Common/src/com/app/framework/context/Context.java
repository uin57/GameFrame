/**
 * 
 */
package com.app.framework.context;


/**
 * @author lisong
 * @version 2013-3-7 下午5:17:59
 */
public interface Context {

	/**http传输协议*/
	int TRANS_PROTO_HTTP = 1;
	/**socket传输协议*/
	int TRANS_PROTO_SOCKET = 2;
	
	byte[] getContent();

	void execute() throws Exception;
	
	int getTransportProtocol();
	
	void close();
	
}
