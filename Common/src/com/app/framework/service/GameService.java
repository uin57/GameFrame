/**
 * 
 */
package com.app.framework.service;



/**
 * service为单例模式<p>
 * service中的所有可被客户端调用的方法必须:
 * 1.被ServiceClientVisible注解注释
 * 2.带唯一参数RequestContext<p>
 * 3.方法的返回值为RequestResult<p>
 * 方法示例:<P>
 * @ClientVisible
 * RequestResult example(RequestContext context)
 * @author lisong
 * @version 2013-3-4 下午5:50:31
 */
public interface GameService {

}
