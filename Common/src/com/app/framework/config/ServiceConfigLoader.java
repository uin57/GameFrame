package com.app.framework.config;

import com.app.framework.service.GameService;
import com.app.framework.singleton.ServiceSingletonManager;


public class ServiceConfigLoader extends AbstractConfigLoader<ServiceConfig,GameService>{
	
	public final static String FILE_NAME="service.xml";
	
	private static ServiceConfigLoader instance;
	
	static{
		instance=new ServiceConfigLoader();
	}
	private ServiceConfigLoader() {
		super(FILE_NAME);
	}
	public static ServiceConfigLoader getInstance() {
		return instance;
	}
	@SuppressWarnings("unchecked")
	@Override
	public void load() throws Exception {
		
		super.init(ServiceConfig.class);
		
		for(GameConfig<GameService> config : getConfigs().values()) {
			//获取所有的method
			Class<GameService> service = (Class<GameService>) Class.forName(config.getValue());
			((ServiceConfig)config).setMethods(loadClassMethods(service));
			ServiceSingletonManager.getInstance().addSingleton(Short.parseShort(config.getId()),(GameService)(Class.forName(config.getValue()).newInstance()));
		}
	}
}
