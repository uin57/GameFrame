package com.app.framework.config;

import com.app.framework.singleton.ActionSingletonManager;
import com.app.service.action.GameAction;

@SuppressWarnings("rawtypes")
public class ActionConfigLoader extends AbstractConfigLoader<ActionConfig,GameAction>{
	
	public final static String FILE_NAME="action.xml";
	
	private static ActionConfigLoader instance;
	
	static{
		instance=new ActionConfigLoader();
	}
	
	private ActionConfigLoader() {
		super(FILE_NAME);
	}
	
	public static ActionConfigLoader getInstance() {
		return instance;
	}
	
	public void load() throws Exception {
		
		super.init(ActionConfig.class);
		
		for(GameConfig<GameAction> config : getConfigs().values()) {
			
			ActionSingletonManager.getInstance().addSingleton(Short.parseShort(config.getId()),(GameAction)(Class.forName(config.getValue()).newInstance()));
			
		}
	}
}
