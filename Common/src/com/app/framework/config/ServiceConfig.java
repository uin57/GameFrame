package com.app.framework.config;

import java.lang.reflect.Method;
import java.util.Map;

import com.app.framework.service.GameService;

public class ServiceConfig extends AbstractGameConfig<GameService> {

	private Map<String,Method> methods;
	
	public Map<String, Method> getMethods() {
		return methods;
	}
	public void setMethods(Map<String, Method> methods) {
		this.methods = methods;
	}
}
