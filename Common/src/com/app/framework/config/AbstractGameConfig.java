/**
 * 
 */
package com.app.framework.config;


/**
 * @author lisong
 * @version 2013-3-29 上午10:52:20
 */
public abstract class AbstractGameConfig<T> implements GameConfig<T> {

	private String id;
	
	private String value;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
