package com.app.framework.config;


public interface GameConfig<T> {

	public String getId();
	
	public void setId(String id);
	
	public  void setValue(String value);
	
	public String getValue();
	
	
}
