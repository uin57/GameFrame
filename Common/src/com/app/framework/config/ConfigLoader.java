/**
 * 
 */
package com.app.framework.config;

import java.util.Map;

/**
 * @author lisong
 * @version 2013-2-18 下午3:34:03
 */
public interface ConfigLoader<T,E> {

	void load() throws Exception;
	
	Map<String,GameConfig<E>> getConfigs();
	
	GameConfig<E> getConfig(String name);
}
