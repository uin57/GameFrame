package com.app.framework.config;


public class PropConfigLoader extends AbstractConfigLoader<PropConfig,Object> {

public final static String FILE_NAME="prop.xml";
	
	private static PropConfigLoader instance;
	
	static{
		instance=new PropConfigLoader();
	}
	
	private PropConfigLoader() {
		super(FILE_NAME);
	}
	
	public static PropConfigLoader getInstance() {
		return instance;
	}
	
	public void load() throws Exception {

		super.init(PropConfig.class);
		
	}
	
	
}
