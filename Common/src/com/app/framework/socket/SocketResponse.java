/**
 * 
 */
package com.app.framework.socket;

import java.util.List;

import org.apache.log4j.Logger;

import com.app.framework.context.Request;
import com.app.framework.context.Response;
import com.app.framework.result.RequestResult;
import com.app.framework.socket.sender.Sender;

/**
 * @author lisong
 * @version 2013-3-4 下午3:27:30
 */
public abstract class SocketResponse<T extends Sender<?>> implements Response<List<Packet>> {

	private static final Logger logger = Logger.getLogger(SocketResponse.class);
	
	private RequestResult<List<Packet>> result;
	
	private T sender;
	
	private Request request;
	
	public SocketResponse(T sender) {
		this.sender = sender;
	}
	@Override
	public void output() throws Exception {
		
		if(result != null && result.getData() != null) {
			sender.sendMessage(result.getData());
		}
	}
	
	@Override
	public void close() {

		request = null;
		
		result = null;
		
		sender = null;
	}

	@Override
	public void setRequestResult(RequestResult<List<Packet>> result) {
		this.result = result;
		
	}

	@Override
	public RequestResult<List<Packet>> getRequestResult() {
		return result;
	}

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	@Override
	public void protocolException(int code, List<Packet> message) {
		
		try {
			
			output();
			
			close();
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
			logger.error(message,e);
		}
	}
	public T getSender() {
		return sender;
	}
}
