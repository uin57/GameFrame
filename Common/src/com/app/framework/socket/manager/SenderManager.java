package com.app.framework.socket.manager;

import java.util.Map;

import com.app.framework.socket.sender.Sender;

public interface SenderManager<K,V extends Sender<K>> {

	Map<K,V> getAllSenders();
	
	V getSender(K k);
	
	V addSender(V v);
	
	boolean removeSender(Sender<K> v);
	
	int size();
}
