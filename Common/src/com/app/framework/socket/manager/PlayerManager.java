package com.app.framework.socket.manager;

import com.app.framework.socket.sender.Player;


public class PlayerManager extends AbstractSenderManager<Long,Player> {

	private PlayerManager() {
		super();
	}
	
	private static PlayerManager instance = new PlayerManager();
	
	public static PlayerManager getInstance() {
		
		return instance;
	}
}
