package com.app.framework.socket.manager;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.app.framework.socket.sender.Sender;

public abstract class AbstractSenderManager<K,V extends Sender<K>> implements SenderManager<K, V> {

	private ConcurrentHashMap<K, V> senders = new ConcurrentHashMap<K, V>();
	
	public AbstractSenderManager() {
//		Thread t = new Thread(new Runnable() {
//			@Override
//			public void run() {
//				
//			}
//		});
//		t.setDaemon(true);
//		t.start();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public V addSender(V v) {
		Object key = v.getId();
		V sender = senders.put((K)key, v);
		return sender;
	}
	
	@Override
	public Map<K, V> getAllSenders() {

		return senders;
	}
	
	@Override
	public V getSender(K k) {
		return senders.get(k);
	}
	
	@Override
	public boolean removeSender(Sender<K> sender) {
		if(sender.getId() == null) {
			return true;
		}
		return senders.remove(sender.getId(), sender);
	}
	
	@Override
	public int size() {
		return senders.size();
	}
}
