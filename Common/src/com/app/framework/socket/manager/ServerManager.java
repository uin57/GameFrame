package com.app.framework.socket.manager;

import com.app.framework.socket.sender.Server;

/**
 * server2server 通信管理
 * @author lisong_otd
 *
 */
public class ServerManager extends AbstractSenderManager<Integer,Server> {

	private ServerManager() {
		super();
	}
	
	private static ServerManager instance = new ServerManager();
	
	public static ServerManager getInstance() {
		
		return instance;
	}
}
