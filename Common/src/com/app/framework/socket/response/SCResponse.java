package com.app.framework.socket.response;

import com.app.framework.socket.SocketResponse;
import com.app.framework.socket.sender.Player;

/**
 * server2client
 * @author lisong_otd
 *
 */
public class SCResponse extends SocketResponse<Player> {

	public SCResponse(Player sender) {
		super(sender);
	}

}
