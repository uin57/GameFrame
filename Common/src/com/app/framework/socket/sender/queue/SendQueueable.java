package com.app.framework.socket.sender.queue;

import org.jboss.netty.channel.Channel;

public interface SendQueueable {

	Channel getChannel();
	
	boolean isChannelActive();
	
	
}
