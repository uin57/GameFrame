package com.app.framework.socket.sender;

import java.util.List;

import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFuture;

import com.app.framework.socket.Packet;
import com.app.framework.socket.manager.SenderManager;

/**
 * 
 * 消息的发送者接口
 * 
 */
public interface Sender<T> {
	
	/** Sender 状态 : 连接刚刚创建 */
	int STATE_CONNECTED = 0;
	
	/** Sender 状态 : 连接已断开 */
	int STATE_DISCONNECTED = 1;

	
	void setId(T t);
	
	T getId();
	/**
	 * 断开连接
	 */
	void disconnect();

	/**
	 * 获取与发送者绑定的channel
	 * 
	 * @param session
	 */
	Channel getChannel();
	
	ChannelFuture sendMessage(Packet... packet);
	
	ChannelFuture sendMessage(List<Packet> packet);
	
	SenderManager<T,? extends Sender<T>> getManager();
	
	boolean isChannelActive();
	
}
