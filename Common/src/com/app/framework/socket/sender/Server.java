package com.app.framework.socket.sender;

import java.util.List;

import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFuture;

import com.app.framework.socket.Packet;
import com.app.framework.socket.manager.ServerManager;
import com.app.framework.socket.sender.queue.SendQueueable;
import com.app.framework.socket.sender.queue.SenderQueue;

public class Server extends AbstractSender<Integer> implements SendQueueable {

	private int type;
	
	private final SenderQueue queue = new SenderQueue(this);
	
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Server(Channel channel) {
		super(channel,ServerManager.getInstance());
	}
	
	@Override
	public ChannelFuture sendMessage(List<Packet> packet) {
		for(int i=0;i<packet.size();i++) {
			queue.add(packet.get(i));
		}
		return null;
	}
	
	@Override
	public ChannelFuture sendMessage(Packet... packet) {
		for(int i=0;i<packet.length;i++) {
			queue.add(packet[i]);
		}
		return null;
	}
	@Override
	protected void close() {
		super.close();
		queue.clear();
	}
}
