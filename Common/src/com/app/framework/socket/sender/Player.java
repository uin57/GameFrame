package com.app.framework.socket.sender;

import org.jboss.netty.channel.Channel;

import com.app.framework.socket.manager.PlayerManager;

public class Player extends AbstractSender<Long> {

	public Player(Channel channel) {
		
		super(channel,PlayerManager.getInstance());
	}
}
