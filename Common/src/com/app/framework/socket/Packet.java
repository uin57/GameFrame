package com.app.framework.socket;

import com.google.protobuf.GeneratedMessage;

public class Packet {

	private short type;
	private byte[] content;
	public Packet(short type,GeneratedMessage.Builder<?> builder) {
		this.type = type;
		GeneratedMessage msg = (GeneratedMessage) builder.build();
		this.content = msg.toByteArray();
	}
	
	public Packet(short type,byte[] content) {
		this.type = type;
		this.content = content;
	}
	
	public short getType() {
		return type;
	}
	public void setType(short type) {
		this.type = type;
	}
	public byte[] getContent() {
		return content;
	}
	public void setContent(byte[] content) {
		this.content = content;
	}
}
