/**
 * 
 */
package com.app.framework.orm.util;

/**
 * @author lisong
 * @version 2013-3-21 下午2:29:20
 */
public enum SimpleFieldType {

	EString {
		@Override
		public Object parse(String data) {
			return data;
		}
	},
	
	EInteger {
		@Override
		public Object parse(String data) {
			
			return Integer.parseInt(data);
		}
	},
	ELong {

		@Override
		public Object parse(String data) {
			
			return Long.parseLong(data);
		}
		
	},
	EDouble {

		@Override
		public Object parse(String data) {
			
			return Double.parseDouble(data);
		}
		
	},
	EFloat {

		@Override
		public Object parse(String data) {
			
			return Float.parseFloat(data);
		}
		
	},
	EShort {

		@Override
		public Object parse(String data) {
			
			return Short.parseShort(data);
		}
		
	},
	EBoolean {

		@Override
		public Object parse(String data) {
			
			return Boolean.parseBoolean(data);
		}
		
	},
	ECharacter {

		@Override
		public Object parse(String data) {
			return data.charAt(0);
		}
		
	},
	EByte {
		@Override
		public Object parse(String data) {

			return Byte.parseByte(data);
		}
	};
	
	public abstract Object parse(String data) ;
}
