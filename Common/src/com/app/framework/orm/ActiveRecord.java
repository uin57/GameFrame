package com.app.framework.orm;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;

import com.app.framework.annotations.Id;
import com.app.framework.annotations.TransferInvisible;
import com.app.framework.annotations.Transient;
import com.app.framework.exception.ORMException;
import com.app.framework.orm.util.ClassHelper;
import com.app.framework.result.Result;
import com.app.framework.util.ReflectionUtil;
import com.app.framework.util.helper.ClassInfo;
import com.app.framework.util.helper.FieldInfo;

public abstract class ActiveRecord implements StorableModel,Result {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3207234908936778242L;

	private static final Logger logger = Logger.getLogger(ActiveRecord.class);

	@Id
	protected long id;
	@Transient
	private String classname = ClassHelper.getRecordClass(getClass()).getName();
	@Transient
	@TransferInvisible
	boolean modified = true;//类是否有修改
	@Transient
	@TransferInvisible
	private boolean transferAllField;//是否传输所有的属性
	/**
	 * key为 getFieldName字符串<br>
	 * value为list<br> 
	 * list第一项为Field对象  第二项又是一个List<br>
	 * 第二个List的第一项为classname其余项为对象uid或者为一个map
	 */
	
	@Transient
	@TransferInvisible
	Map<String,List<?>> referenceJson = new HashMap<String, List<?>>();
	
	@Transient
	@TransferInvisible
	Set<String> changed = new HashSet<String>();//放入所有被调用set后的方法

	public String getClassname() {
		return classname;
	}
	
	public boolean modified_get() {
		return modified;
	}

	public void modified_set() {
		this.modified = true;
	}
	public void notModified_set() {
		this.modified = false;
	}
	/**
	 * 设置传输所有数据
	 * @param transferAllField
	 */
	public void transferAllField() {
		this.transferAllField = true;
	}

	@Override
	public long getId() {
		return id;
	}

	@Override
	public void setId(long id) {
		this.id = id;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Serializable asResult() {
		Map<String,Object> map=new HashMap<String,Object>();
		ClassInfo classInfo = null;
		try {
			classInfo = ReflectionUtil.getClassInfo(getClass());
		} catch (ORMException e1) {
			e1.printStackTrace();
			logger.error(e1);
		}
		if(classInfo != null) {
			Map<String,FieldInfo> allFields=classInfo.getTransferFields();
			if(allFields != null) {
				Object fieldValue=null;
				for(FieldInfo fieldInfo:allFields.values()) {
					if(fieldInfo != null) {
						Field f=fieldInfo.getField();
						try {
							//判断是否需要下发
							if(!transferAllField && ClassHelper.isCglibProxyClass(getClass())) {
								String methodName = ClassHelper.genSetMethodNameByField(fieldInfo.getField());
								if(!classInfo.getIdField().getField().getName().equals(f.getName()) && !f.getName().equals("classname")) {//如果不是key 不是classname 则应用transfer判断
									if(!changed.contains(methodName)) {
										continue;
									}
								}
							}
//							if(fieldInfo.getModifyField()!= null) {
//								if(!fieldInfo.getModifyField().getBoolean(this)) {
//									continue;
//								}
//							}
							f.setAccessible(true);
							fieldValue=f.get(this);
							if(fieldValue == null) {
								continue;
							}
							//判断fieldValue类型
							if(ClassHelper.isSimpleClass(fieldValue.getClass())) {
								map.put(f.getName(), fieldValue);
							}
							else if(ClassHelper.isList(fieldValue.getClass())) {
								List<Object> tmp = new ArrayList<Object>();
								for(Object obj : (List<?>)fieldValue) {
									if(obj == null) {
										continue;
									}
									if(ActiveRecord.class.isAssignableFrom(obj.getClass())) {
										tmp.add(((ActiveRecord)obj).getId());//uid
									}
									else {
										tmp.add(obj);
									}
								}
								map.put(f.getName(), tmp);
							}
							else if(ClassHelper.isMap(fieldValue.getClass())) {
								Map<Object,Object> tmp = new HashMap<Object, Object>();
								for(Entry<Object, Object> e : ((Map<Object,Object>)fieldValue).entrySet()) {
									if(e.getValue() == null) {
										continue;
									}
									if(ActiveRecord.class.isAssignableFrom(e.getValue().getClass())) {
										tmp.put(e.getKey(),((ActiveRecord)e.getValue()).getId());
									}
									else {
										tmp.put(e.getKey(),e.getValue());
									}
								}
								map.put(f.getName(), tmp);
							}
							else if(ActiveRecord.class.isAssignableFrom(fieldValue.getClass())) {
								map.put(f.getName(), ((ActiveRecord)fieldValue).getId());
							}
							else {
								map.put(f.getName(), fieldValue);
							}
						}catch (Exception e) {
							e.printStackTrace();
							logger.error(e);
						}
					}
		    	}
	    	  }
		}
		return (Serializable) map;
	}
	
	@Override
	public boolean isChanged(Field field) {
		if(!modified) {
			return false;
		}
		String setFieldName = ClassHelper.genSetMethodNameByField(field);
		return changed.contains(setFieldName);
	}
}
