/**
 * 
 */
package com.app.framework.orm;


/**
 * @author lisong
 * @version 2013-4-28 上午10:16:54
 */
public abstract class CollectionItem extends ActiveRecord {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3552405175137322728L;
	
	/**
	 * 对应集合的uid
	 */
	private long collectionId;

	public long getCollectionId() {
		return collectionId;
	}

	public void setCollectionId(long collectionId) {
		this.collectionId = collectionId;
	}

}
