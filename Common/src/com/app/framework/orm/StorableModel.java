/**
 * 
 */
package com.app.framework.orm;

import java.io.Serializable;
import java.lang.reflect.Field;

/**
 * @author lisong
 * @version 2013-3-27 上午10:06:13
 */
public interface StorableModel extends Serializable {
	
	public void setId(long id);
	
	public long getId();
	
	public String getClassname();
	
	public boolean modified_get();
	
	public void modified_set();
	
	public void notModified_set();
	
	public boolean isChanged(Field field);

}
