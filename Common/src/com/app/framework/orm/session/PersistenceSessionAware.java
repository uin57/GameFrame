/**
 * 
 */
package com.app.framework.orm.session;


/**
 * @author lisong
 * @version 2013-3-26 下午3:01:36
 */
public interface PersistenceSessionAware {

	public void setPersistenceSession(PersistenceSession persistenceSession);
}
