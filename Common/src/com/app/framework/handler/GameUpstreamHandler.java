/**
 * 
 */
package com.app.framework.handler;

import org.jboss.netty.channel.SimpleChannelUpstreamHandler;

/**
 * @author lisong
 * @version 2013-2-26 下午5:52:11
 */
public abstract class GameUpstreamHandler extends SimpleChannelUpstreamHandler {

	/**
	 * 客户端到服务器
	 */
	public static final int HANDLER_TYPE_CS = 1;
	/**
	 * 服务器到服务器
	 */
	public static final int HANDLER_TYPE_SS = 2;
	
	protected final int handlerType;
	
	protected final boolean forClient;
	
	public GameUpstreamHandler(int handlerType,boolean forClient) {
		this.handlerType = handlerType;
		this.forClient = forClient;
	}
	/**
	 * 请求字符串的名称
	 */
	public static final String REQUEST_QUERY_STRING = "cmd";
	/**
	 * 请求的action名字
	 */
	public static final String REQUEST_ACTION_NAME_STRING  = "action";
	/**
	 * 请求的service名字
	 */
	public static final String REQUEST_SERVICE_NAME_STRING  = "service";
	/**
	 * 请求的service方法
	 */
	public static final String REQUEST_METHOD_NAME_STRING  = "method";
	
	public static final String REQUEST_USERUID_STRING  = "user";
	
	public static final String REQUEST_SESSIONID_STRING  = "session";
	
	public static final String REQUEST_ID_STRING  = "reqid";
	
	/**
	 * 第三方平台回调url
	 */
	public static final String REQUEST_URI_PLATFORM = "/platform";

	public boolean isForClient() {
		return forClient;
	}
}
