/**
 * 
 */
package com.app.framework.singleton;

import com.app.service.action.GameAction;

/**
 * @author lisong
 * @version 2013-3-8 下午2:35:17
 */
public class ActionSingletonManager extends AbstractSingletonManager<GameAction<?>> {

	private static ActionSingletonManager instance = new ActionSingletonManager();
	
	public static ActionSingletonManager getInstance() {
		
		return instance;
	}
	
	private ActionSingletonManager() {
		
	}

}
