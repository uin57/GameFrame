/**
 * 
 */
package com.app.framework.singleton;


/**
 * @author lisong
 * @version 2013-3-8 下午2:25:33
 */
public interface SingletonManager<T> {
	
	T getSingleton(short id);
	
	void addSingleton(short id,T t);
	
}
