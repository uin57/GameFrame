/**
 * 
 */
package com.app.framework.singleton;

import com.app.framework.service.GameService;

/**
 * @author lisong
 * @version 2013-3-8 下午2:34:37
 */
public class ServiceSingletonManager extends AbstractSingletonManager<GameService> {

	
	private static ServiceSingletonManager instance = new ServiceSingletonManager();
	
	public static ServiceSingletonManager getInstance() {
		
		return instance;
	}
	
	private ServiceSingletonManager() {
		
	}
	
}
