/**
 * 
 */
package com.app.framework.singleton;

import java.util.Map;

import org.jboss.netty.util.internal.ConcurrentHashMap;

import com.app.framework.exception.ConfigException;

/**
 * @author lisong
 * @version 2013-3-8 下午2:31:19
 */
public abstract class AbstractSingletonManager<T> implements SingletonManager<T> {

	/**
	 * key service.xml或者action.xml中定义的id
	 */
	private Map<Short,T> singletons = new ConcurrentHashMap<Short, T>();
	
	public void addSingleton(short id,T t) {
		if(singletons.containsKey(id)) {
			throw new ConfigException("singleton id already exists id:"+id);
		}
		singletons.put(id, t);
	}
	
	@Override
	public T getSingleton(short id) {

		return singletons.get(id);
	}
}
