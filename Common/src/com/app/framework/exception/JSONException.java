package com.app.framework.exception;

public class JSONException extends FrameException {
	private static final long serialVersionUID = 1L;
	
	public JSONException(String message) {
		super(message);
	}

	public JSONException(String message, Throwable throwable) {
		super(message, throwable);
	}
}