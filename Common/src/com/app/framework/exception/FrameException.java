/**
 * 
 */
package com.app.framework.exception;

/**
 * @author lisong
 * @version 2013-2-28 上午11:15:03
 */
public class FrameException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3690506576603492036L;

	protected final int code;
	
	public FrameException(String msg) {
		super(msg);
		code = 400;
	}
	
	public FrameException(String msg,int code) {
		super(msg);
		this.code = code;
	}
	
	public FrameException(Throwable e) {
		super(e);
		code = 400;
	}
	
	public FrameException(Throwable e,int code) {
		super(e);
		this.code = code;
	}
	
	public FrameException(String msg,Throwable e) {
		super(msg,e);
		code = 400;
	}
	
	public FrameException(String msg,Throwable e,int code) {
		super(msg,e);
		this.code = code;
	}

	public int getCode() {
		return code;
	}
}
