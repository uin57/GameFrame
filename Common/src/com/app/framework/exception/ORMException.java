package com.app.framework.exception;

public class ORMException extends FrameException{
	private static final long serialVersionUID = 1L;
	
	public ORMException(String str) {
		super(str);
	}
	public ORMException(String str,Throwable ex) {
		super(str, ex);
	}
	public ORMException(Throwable ex){
		super(ex);
	}
	public ORMException(String str,int code) {
		super(str,code);
	}
}
