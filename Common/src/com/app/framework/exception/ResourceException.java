/**
 * 
 */
package com.app.framework.exception;

/**
 * @author lisong
 * @version 2013-2-28 上午11:15:03
 */
public class ResourceException extends FrameException {


	/**
	 * 
	 */
	private static final long serialVersionUID = -6020004383136106864L;

	public ResourceException(String msg) {
		super(msg);
	}
	
	public ResourceException(Throwable e) {
		super(e);
	}
	
	public ResourceException(String msg,Throwable e) {
		super(msg,e);
	}
}
