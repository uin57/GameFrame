/**
 * 
 */
package com.app.framework.exception;

/**
 * @author lisong
 * @version 2013-2-28 上午11:15:03
 */
public class ConfigException extends FrameException {


	/**
	 * 
	 */
	private static final long serialVersionUID = -6020004383136106864L;

	public ConfigException(String msg) {
		super(msg);
	}
	
	public ConfigException(Throwable e) {
		super(e);
	}
	
	public ConfigException(String msg,Throwable e) {
		super(msg,e);
	}
}
