/**
 * 
 */
package com.app.framework.exception;

/**
 * @author lisong
 * @version 2013-2-28 上午11:15:03
 */
public class CommunicationException extends FrameException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3690506576603492036L;

	public CommunicationException(String msg) {
		super(msg);
	}
	
	public CommunicationException(Throwable e) {
		super(e);
	}
	
	public CommunicationException(String msg,Throwable e) {
		super(msg,e);
	}
}
