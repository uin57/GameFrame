/**
 * 
 */
package com.app.framework.exception;

/**
 * @author lisong
 * @version 2013-2-28 上午11:15:03
 */
public class ClientException extends FrameException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3690506576603492036L;

	public ClientException(String msg) {
		super(msg,400);
	}
	
	public ClientException(String msg,int code) {
		super(msg,code);
	}
	
	public ClientException(Throwable e) {
		super(e,400);
	}
	
	public ClientException(Throwable e,int code) {
		super(e,code);
	}
	
	public ClientException(String msg,Throwable e) {
		super(msg,e,400);
	}
	
	public ClientException(String msg,Throwable e,int code) {
		super(msg,e,code);
	}
}
