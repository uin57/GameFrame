/**
 * 
 */
package com.app.framework.exception;

/**
 * @author lisong
 * @version 2013-2-28 上午11:15:03
 */
public class ServerException extends FrameException {


	/**
	 * 
	 */
	private static final long serialVersionUID = -6020004383136106864L;

	public ServerException(String msg) {
		super(msg);
	}
	
	public ServerException(Throwable e) {
		super(e);
	}
	
	public ServerException(String msg,Throwable e) {
		super(msg,e);
	}
}
